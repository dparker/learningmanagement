﻿Partial Public Class cwTest
    Inherits ChildWindow
    Public ProgramID As String = ""
    Dim WithEvents w As cwQuestion
    '    Dim s As List(Of ItemRecord)

    Public Sub New()
        InitializeComponent()
    End Sub
    Public Sub Setup()
        GetClasses()
        If ProgramID = "" Then
            Button1.Content = "Save"
            DataGrid1.IsEnabled = False
            OKButton.IsEnabled = False
            bSampleTest.IsEnabled = False
        Else
            Button1.Content = "Edit"
            tTitle.IsEnabled = False
            cbClass.IsEnabled = False
            cbType.IsEnabled = False
        End If
    End Sub

    Private Sub OKButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles OKButton.Click
        w = New cwQuestion
        w.TestID = ProgramID
        w.Show()
        'w = Nothing
    End Sub

    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles CancelButton.Click
        Me.DialogResult = False
    End Sub
    Private Sub Button_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs)
        w = New cwQuestion
        w.TestID = ProgramID
        w.tQuestion.Text = CType(DataGrid1.SelectedItem, ClassRecord).Title.ToString
        w.QuestionID = CType(DataGrid1.SelectedItem, ClassRecord).ID.ToString
        w.Setup()
        w.Show()
        'w = Nothing
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles Button1.Click
        Dim sClassID As String = "0"
        Dim sType As String = "C"
        If cbType.SelectedIndex = 1 Then sType = "T"
        If cbClass.SelectedIndex > -1 Then sClassID = CType(cbClass.SelectedItem, ClassRecord).ID.ToString
        If ProgramID = "" And Button1.Content = "Save" Then
            Me.Cursor = Cursors.Wait
            Dim q As New ATIData.SvcSoapClient
            AddHandler q.XMLQueryCompleted, AddressOf GetNewID
            q.XMLQueryAsync("Insert into tests (testname,testtype,CourseID) values ('" + tTitle.Text.Replace("'", "''") + "','" + sType + "'," + sClassID + ") select ID from Tests where ID = @@Identity", "Bubba985")
        ElseIf ProgramID <> "" And Button1.Content = "Save" Then
            Dim q As New ATIData.SvcSoapClient
            If cbClass.Visibility = Windows.Visibility.Collapsed Then
                q.DataNonQueryAsync("Update tests set testname='" + tTitle.Text.Replace("'", "''") + "',testtype='" + IIf(cbType.SelectedIndex = 0, "C", "T") + "',courseid=0 where id=" + ProgramID, "Bubba985")
            Else
                q.DataNonQueryAsync("Update tests set testname='" + tTitle.Text.Replace("'", "''") + "',testtype='" + IIf(cbType.SelectedIndex = 0, "C", "T") + "',courseid=" + IIf(cbClass.Visibility = Windows.Visibility.Visible, CType(cbClass.SelectedItem, ClassRecord).ID.ToString, "0") + " where id=" + ProgramID, "Bubba985")
            End If
            Button1.Content = "Edit"
            DataGrid1.IsEnabled = True
            OKButton.IsEnabled = True
            cbClass.IsEnabled = False
            tTitle.IsEnabled = False
            cbType.IsEnabled = False
            bSampleTest.IsEnabled = True
        Else
            Button1.Content = "Save"
            DataGrid1.IsEnabled = False
            OKButton.IsEnabled = False
            cbClass.IsEnabled = True
            tTitle.IsEnabled = True
            cbType.IsEnabled = True
        End If

    End Sub
    Private Sub GetNewID(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs)
        ProgramID = e.Result.Value.ToString
        Me.Cursor = Cursors.Arrow
        Button1.Content = "Edit"
        DataGrid1.IsEnabled = True
        OKButton.IsEnabled = True
        cbClass.IsEnabled = False
        tTitle.IsEnabled = False
        cbType.IsEnabled = False
        bSampleTest.IsEnabled = True
    End Sub
    Private Sub GetClasses()
        Dim q As New ATIData.SvcSoapClient
        AddHandler q.XMLQueryCompleted, AddressOf GetNewItemsHandler
        q.XMLQueryAsync("Select seminartitle,ID from courses order by Class,seminartitle", "Bubba985")
    End Sub
    Private Sub GetNewItemsHandler(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs)
        Dim c As List(Of ClassRecord)
        c = (From x In e.Result.Elements Select New ClassRecord With {.ID = x.Element("ID").Value.ToString, .Title = x.Element("seminartitle").Value}).ToList
        cbClass.ItemsSource = c
        If ProgramID <> "" Then GetTest()
    End Sub
    Class ClassRecord
        Property ID As String
        Property Title As String
    End Class
    Private Sub GetTest()
        Dim q As New ATIData.SvcSoapClient
        AddHandler q.XMLQueryCompleted, AddressOf GetTestHandler
        q.XMLQueryAsync("Select testtype,courseid from tests where id = " + ProgramID, "Bubba985")
    End Sub
    Private Sub GetTestHandler(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs)
        Dim sCourseID As String = ""
        Dim sTestType As String = "C"
        Dim cr As ClassRecord
        For Each x In e.Result.Elements
            sCourseID = x.Element("courseid").Value.ToString
            sTestType = x.Element("testtype").Value.ToString
        Next
        If sTestType <> "C" Then cbType.SelectedIndex = 1
        For Each cr In cbClass.ItemsSource
            If cr.ID = sCourseID Then
                cbClass.SelectedItem = cr
                Exit For
            End If
        Next
        GetQuestions()
    End Sub
    Private Sub GetQuestions()
        Dim q As New ATIData.SvcSoapClient
        AddHandler q.XMLQueryCompleted, AddressOf GetQuestionHandler
        q.XMLQueryAsync("Select question,ID from testquestions where testid=" + ProgramID + " order by id", "Bubba985")
    End Sub
    Private Sub GetQuestionHandler(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs)
        Dim c As List(Of ClassRecord)
        c = (From x In e.Result.Elements Select New ClassRecord With {.ID = x.Element("ID").Value.ToString, .Title = x.Element("question").Value}).ToList
        DataGrid1.ItemsSource = c
        lQuestionCount.Content = "Question Count: " + c.Count.ToString

    End Sub

    Private Sub cbType_SelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Controls.SelectionChangedEventArgs) Handles cbType.SelectionChanged
        cbClass.Visibility = IIf(cbType.SelectedIndex = 0, Visibility.Visible, Visibility.Collapsed)
        Label3.Visibility = IIf(cbType.SelectedIndex = 0, Visibility.Visible, Visibility.Collapsed)
    End Sub
    Private Sub w_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles w.Closed
        GetQuestions()
    End Sub
    Private Sub Button_Click_1(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs)
        Dim sQuestionID As String = CType(DataGrid1.SelectedItem, ClassRecord).ID.ToString
        Dim q As New ATIData.SvcSoapClient
        q.DataNonQueryAsync("delete from testquestions where id=" + sQuestionID, "Bubba985")
        q.DataNonQueryAsync("delete from testanswers where questionid=" + sQuestionID, "Bubba985")
        GetQuestions()
    End Sub

    Private Sub bSampleTest_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles bSampleTest.Click
        System.Windows.Browser.HtmlPage.Window.Navigate(New Uri("http://share.autotraining.net/learning/test.aspx?CustomerID=0&TestID=" + ProgramID), "_newWindow", "toolbar=1,menubar=1,resizable=1,scrollbars=1,top=0,left=0")
    End Sub
End Class
