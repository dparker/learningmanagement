﻿Imports System.Reflection
Partial Public Class MainMenu
    Inherits Page
    Dim a As New ProgramManagement
    Public Sub New()
        InitializeComponent()
        Dim a As Assembly = Assembly.GetExecutingAssembly()
        Dim s As String()
        s = a.FullName.ToString.Split(",")
        lVersion.Content = s(1)
    End Sub

    'Executes when the user navigates to this page.
    Protected Overrides Sub OnNavigatedTo(ByVal e As System.Windows.Navigation.NavigationEventArgs)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles Button1.Click
        a.Type = "P"
        a.Setup()
        Me.Frame1.Content = a
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles Button2.Click
        a.Type = "T"
        a.Setup()
        Me.Frame1.Content = a
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles Button3.Click
        Dim b As New cwConfigureBonuses
        b.Setup()
        b.Show()
        b = Nothing
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles Button4.Click
        Dim b As New cwCustomerBonuses
        b.Show()
        b = Nothing

    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles Button5.Click
        Dim b As New cwMapShops
        b.Show()
        b = Nothing

    End Sub

    Private Sub Button6_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles Button6.Click
        Dim b As New cwFindShop(cwFindShop.eSearchType.Portal)
        AddHandler b.Closed, AddressOf NavigateToPage
        b.Show()
        b = Nothing
    End Sub
    Private Sub NavigateToPage(o As Object, e As EventArgs)
        If CType(o, cwFindShop).ShopID <> "" Then System.Windows.Browser.HtmlPage.Window.Navigate(New Uri("http://share.autotraining.net/learning/Default.aspx?PortalShopID=" + CType(o, cwFindShop).ShopID), "_newWindow", "toolbar=1,menubar=1,resizable=1,scrollbars=1,top=0,left=0")
    End Sub
End Class
