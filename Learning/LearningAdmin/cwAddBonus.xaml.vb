﻿Partial Public Class cwAddBonus
    Inherits ChildWindow
    Public sCustID As String
    Public Sub New()
        InitializeComponent()
        GetItems()
    End Sub

    Private Sub OKButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles OKButton.Click
        Dim q As New ATIData.SvcSoapClient
        AddHandler q.DataNonQueryWithErrorCompleted, AddressOf ItemSaved
        q.DataNonQueryWithErrorAsync("Insert into LearningCustomerBonuses (CustomerID,BonusID,Points) values (" + sCustID + "," + CType(ComboBox1.SelectedItem, BonusItem).ID.ToString + "," + tPoints.Text + ")", "Bubba985")
    End Sub
    Private Sub ItemSaved()
        Me.DialogResult = True
    End Sub
    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles CancelButton.Click
        Me.DialogResult = False
    End Sub
    Public Class BonusItem
        Public Sub New()

        End Sub
        Public Property ID As Integer
        Public Property Name As String
        Public Property Points As Integer
    End Class
    Private Sub GetItems()
        Dim q As New ATIData.SvcSoapClient
        AddHandler q.XMLQueryCompleted, AddressOf GetNewItemsHandler
        q.XMLQueryAsync("Select * from LearningProgramBonuses", "Bubba985")
    End Sub
    Private Sub GetNewItemsHandler(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs)
        Dim s As List(Of BonusItem)
        s = (From x In e.Result.Elements Select New BonusItem With {.ID = x.Element("ID").Value.ToString, .Name = x.Element("BonusName").Value.ToString, .Points = x.Element("BonusPoints").Value}).ToList
        s.Insert(0, New BonusItem With {.Name = "Select"})
        ComboBox1.ItemsSource = s
        ComboBox1.DisplayMemberPath = "Name"
        ComboBox1.SelectedIndex = 0
    End Sub
    Private Sub ComboBox1_SelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Controls.SelectionChangedEventArgs) Handles ComboBox1.SelectionChanged
        tPoints.Text = CType(ComboBox1.SelectedItem, BonusItem).Points.ToString
    End Sub
End Class
