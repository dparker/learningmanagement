﻿Partial Public Class cwQuestion
    Inherits ChildWindow
    Public QuestionID As String
    Public TestID As String
    Public Sub New()
        InitializeComponent()
    End Sub
    Public Sub Setup()
        If QuestionID <> "" Then
            Getanswers()
        End If
    End Sub
    Private Sub Getanswers()
        Dim q As New ATIData.SvcSoapClient
        AddHandler q.XMLQueryCompleted, AddressOf GetAnswersHandler
        q.XMLQueryAsync("Select answer,correct from testanswers where questionid = " + QuestionID + " order by id", "Bubba985")
    End Sub
    Private Sub GetAnswersHandler(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs)
        Dim y As Integer = 1
        Dim sAnswer As String
        Dim bCorrect As Boolean
        For Each x In e.Result.Elements
            sAnswer = x.Element("answer").Value.ToString
            bcorrect = x.Element("correct").Value
            If y = 1 Then
                tResponse1.Text = sAnswer
                rb1.IsChecked = bCorrect
            ElseIf y = 2 Then
                tResponse2.Text = sAnswer
                rb2.IsChecked = bCorrect
            ElseIf y = 3 Then
                tResponse3.Text = sAnswer
                rb3.IsChecked = bCorrect
            ElseIf y = 4 Then
                tResponse4.Text = sAnswer
                rb4.IsChecked = bCorrect
            ElseIf y = 5 Then
                tResponse5.Text = sAnswer
                rb5.IsChecked = bCorrect
            ElseIf y = 6 Then
                tResponse6.Text = sAnswer
                rb6.IsChecked = bCorrect
            End If
            y = y + 1
        Next
    End Sub
    Private Sub OKButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles OKButton.Click
        Dim q As New ATIData.SvcSoapClient
        Dim sQuery As String = ""
        If QuestionID = "" Then
            AddHandler q.XMLQueryCompleted, AddressOf GetNewID
            q.XMLQueryAsync("Insert into testquestions (TestID,Question) values (" + TestID + ",'" + tQuestion.Text.Replace("'", "''") + "') select ID from TestQuestions where ID = @@Identity", "Bubba985")
            Me.Cursor = Cursors.Wait
            Exit Sub
        Else
            AddHandler q.DataNonQueryCompleted, AddressOf QuestionUpdated
            sQuery = "update testquestions set Question='" + tQuestion.Text.Replace("'", "''") + "' where id=" + QuestionID + " "
            sQuery = sQuery + "Delete from testanswers where questionid=" + QuestionID
            q.DataNonQueryAsync(sQuery, "Bubba985")
            'q.DataNonQueryAsync("update testquestions set Question='" + tQuestion.Text.Replace("'", "''") + "' where id=" + QuestionID, "Bubba985")
            'q.DataNonQueryAsync("Delete from testanswers where questionid=" + QuestionID, "Bubba985")
            'SaveResponses()
        End If
        '        Me.DialogResult = True
        End Sub
    Private Sub QuestionUpdated()
        SaveResponses()
    End Sub

    Private Sub GetNewID(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs)
        QuestionID = e.Result.Value.ToString
        SaveResponses()
    End Sub
        Private Sub SaveResponses()
        Dim q As New ATIData.SvcSoapClient
        Dim sQuery As String = ""
            If tResponse1.Text.Trim <> "" Then
            'q.DataNonQueryAsync("Insert into testanswers (QuestionID,Answer,Correct) values (" + QuestionID + ",'" + tResponse1.Text.Replace("'", "''") + "'," + IIf(rb1.IsChecked, 1, 0).ToString + ")", "Bubba985")
            sQuery = sQuery + "Insert into testanswers (QuestionID,Answer,Correct) values (" + QuestionID + ",'" + tResponse1.Text.Replace("'", "''") + "'," + IIf(rb1.IsChecked, 1, 0).ToString + ") "
            End If
            If tResponse2.Text.Trim <> "" Then
            'q.DataNonQueryAsync("Insert into testanswers (QuestionID,Answer,Correct) values (" + QuestionID + ",'" + tResponse2.Text.Replace("'", "''") + "'," + IIf(rb2.IsChecked, 1, 0).ToString + ")", "Bubba985")
            sQuery = sQuery + "Insert into testanswers (QuestionID,Answer,Correct) values (" + QuestionID + ",'" + tResponse2.Text.Replace("'", "''") + "'," + IIf(rb2.IsChecked, 1, 0).ToString + ") "
        End If
            If tResponse3.Text.Trim <> "" Then
            'q.DataNonQueryAsync("Insert into testanswers (QuestionID,Answer,Correct) values (" + QuestionID + ",'" + tResponse3.Text.Replace("'", "''") + "'," + IIf(rb3.IsChecked, 1, 0).ToString + ")", "Bubba985")
            sQuery = sQuery + "Insert into testanswers (QuestionID,Answer,Correct) values (" + QuestionID + ",'" + tResponse3.Text.Replace("'", "''") + "'," + IIf(rb3.IsChecked, 1, 0).ToString + ") "
        End If
            If tResponse4.Text.Trim <> "" Then
            'q.DataNonQueryAsync("Insert into testanswers (QuestionID,Answer,Correct) values (" + QuestionID + ",'" + tResponse4.Text.Replace("'", "''") + "'," + IIf(rb4.IsChecked, 1, 0).ToString + ")", "Bubba985")
            sQuery = sQuery + "Insert into testanswers (QuestionID,Answer,Correct) values (" + QuestionID + ",'" + tResponse4.Text.Replace("'", "''") + "'," + IIf(rb4.IsChecked, 1, 0).ToString + ") "
        End If
            If tResponse5.Text.Trim <> "" Then
            'q.DataNonQueryAsync("Insert into testanswers (QuestionID,Answer,Correct) values (" + QuestionID + ",'" + tResponse5.Text.Replace("'", "''") + "'," + IIf(rb5.IsChecked, 1, 0).ToString + ")", "Bubba985")
            sQuery = sQuery + "Insert into testanswers (QuestionID,Answer,Correct) values (" + QuestionID + ",'" + tResponse5.Text.Replace("'", "''") + "'," + IIf(rb5.IsChecked, 1, 0).ToString + ") "
        End If
            If tResponse6.Text.Trim <> "" Then
            'q.DataNonQueryAsync("Insert into testanswers (QuestionID,Answer,Correct) values (" + QuestionID + ",'" + tResponse6.Text.Replace("'", "''") + "'," + IIf(rb6.IsChecked, 1, 0).ToString + ")", "Bubba985")
            sQuery = sQuery + "Insert into testanswers (QuestionID,Answer,Correct) values (" + QuestionID + ",'" + tResponse6.Text.Replace("'", "''") + "'," + IIf(rb6.IsChecked, 1, 0).ToString + ") "
        End If
        AddHandler q.DataNonQueryCompleted, AddressOf ResponsesSaved
        q.DataNonQueryAsync(sQuery, "Bubba985")
        End Sub
    Private Sub ResponsesSaved()
        Me.DialogResult = True
    End Sub
    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles CancelButton.Click
        Me.DialogResult = False
    End Sub

        Private Sub ChildWindow_Closed(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Closed

        End Sub
    End Class
