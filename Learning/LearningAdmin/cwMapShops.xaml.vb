﻿Imports System.Xml
Partial Public Class cwMapShops
    Inherits ChildWindow
    Dim WithEvents w As cwFindShop
    Public Sub New()
        InitializeComponent()
        PopulatePrograms()
    End Sub

    Private Sub OKButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles OKButton.Click
        Me.DialogResult = True
    End Sub

    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles CancelButton.Click
        Me.DialogResult = False
    End Sub

    Private Sub bSearchOET_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles bSearchOET.Click
        w = New cwFindShop(cwFindShop.eSearchType.OET)
        w.Show()
    End Sub

    Private Sub w_Closed(sender As Object, e As System.EventArgs) Handles w.Closed
        If w.ShopID = "" Then Exit Sub
        Dim sProgramID As String
        If w._SearchType = cwFindShop.eSearchType.OET Then
            lOName.Content = w.ShopName
            lOAddress.Content = w.Address
            lOCSZ.Content = w.CSZ
            lOPhone.Content = w.Phone
            lOName.Content = w.ShopName
            lOID.Content = w.ShopID
            If w.ProgramID Is Nothing Then
                sProgramID = "0"
            Else
                sProgramID = w.ProgramID.ToString
            End If
            cbPrograms.SelectedItem = CType(cbPrograms.ItemsSource, List(Of ProgramRecord)).FirstOrDefault(Function(o) o.ID = sProgramID)
            dpStart.DisplayDate = w.StartDate.ToShortDateString
            dpStart.SelectedDate = w.StartDate.ToShortDateString
            dpEnd.DisplayDate = w.EndDate.ToShortDateString
            dpEnd.SelectedDate = w.EndDate.ToShortDateString
            Me.Cursor = Cursors.Wait
            CheckOET()
        Else
            lPName.Content = w.ShopName
            lPAddress.Content = w.Address
            lPCSZ.Content = w.CSZ
            lPPhone.Content = w.Phone
            lPName.Content = w.ShopName
            lPID.Content = w.ShopID
            CheckPortal()
        End If
    End Sub
    Private Sub CheckPortal()
        Dim d As New ATIData.SvcSoapClient
        AddHandler d.XMLQueryCompleted, AddressOf CheckPortalXMLQueryCompleted
        d.XMLQueryAsync("Select OETCustomerID,PortalShopID FROM PortalOETCustomerLinks where portalshopid=" + lPID.Content.ToString, "Bubba985")
    End Sub
    Private Sub CheckPortalXMLQueryCompleted(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs)
        If e.Result.Elements.Count > 0 Then
            Dim o As New ATIData.SvcSoapClient
            AddHandler o.XMLQueryCompleted, AddressOf O2XMLQueryCompletedCompleted
            o.XMLQueryAsync("select top 1 id,name,address,city,state,zip,phone1 as phone,LearningProgramID,CONVERT(VARCHAR(10),LearningProgramStartDate,110) as StartDate,CONVERT(VARCHAR(10),LearningProgramEndDate,110) as EndDate from customers where id=" + CType(e.Result.Nodes(0), System.Xml.Linq.XElement).Element("OETCustomerID").Value.ToString, "Bubba985")
        Else
            If lOID.Content <> "" And lPID.Content <> "" Then
                bLink.Content = "<< Link >>"
                bLink.SetValue(Control.BackgroundProperty, New SolidColorBrush(Colors.Green))
                bLink.SetValue(Control.BorderBrushProperty, New SolidColorBrush(Colors.Green))
                bLink.Visibility = Windows.Visibility.Visible
            Else
                bLink.Visibility = Windows.Visibility.Collapsed
            End If
            Me.Cursor = Cursors.Arrow
        End If
    End Sub
    Private Sub O2XMLQueryCompletedCompleted(sender As Object, e As ATIData.XMLQueryCompletedEventArgs)
        Dim s As List(Of CustomerRecord) = (From x In e.Result.Elements Select New CustomerRecord With {.Address = x.Element("address").Value, .ID = x.Element("id").Value, .Name = x.Element("name").Value, .City = x.Element("city").Value + ", " + x.Element("state").Value + " " + x.Element("zip").Value, .Phone = x.Element("phone").Value, .ProgramID = x.Element("LearningProgramID"), .Startdate = x.Element("StartDate"), .Enddate = x.Element("EndDate")}).ToList
        Dim sProgramID As String
        If s.Count > 0 Then
            lOName.Content = s(0).Name
            lOAddress.Content = s(0).Address
            lOCSZ.Content = s(0).City + ", " + s(0).State + " " + s(0).Zip
            lOPhone.Content = s(0).Phone
            lOID.Content = s(0).ID
            bLink.Content = "Unlink"
            bLink.SetValue(Control.BackgroundProperty, New SolidColorBrush(Colors.Red))
            bLink.SetValue(Control.BorderBrushProperty, New SolidColorBrush(Colors.Red))
            bLink.Visibility = Windows.Visibility.Visible
            If s(0).ProgramID Is Nothing Then
                sProgramID = "0"
            Else
                sProgramID = s(0).ProgramID.ToString
            End If
            cbPrograms.SelectedItem = CType(cbPrograms.ItemsSource, List(Of ProgramRecord)).FirstOrDefault(Function(o) o.ID = sProgramID)
            dpStart.DisplayDate = s(0).Startdate.ToShortDateString
            dpStart.SelectedDate = s(0).Startdate.ToShortDateString
            dpEnd.DisplayDate = s(0).Enddate.ToShortDateString
            dpEnd.SelectedDate = s(0).Enddate.ToShortDateString
        End If
        Me.Cursor = Cursors.Arrow
    End Sub
    Private Sub CheckOET()
        Dim d As New ATIData.SvcSoapClient
        AddHandler d.XMLQueryCompleted, AddressOf CheckOETXMLQueryCompleted
        d.XMLQueryAsync("Select OETCustomerID,PortalShopID FROM PortalOETCustomerLinks where oetcustomerid=" + lOID.Content.ToString, "Bubba985")
    End Sub
    Private Sub CheckOETXMLQueryCompleted(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs)
        If e.Result.Elements.Count > 0 Then
            Dim p2 As New PortalData.PortalDataSoapClient
            AddHandler p2.GetDataCompleted, AddressOf P2GetDataCompleted
            p2.GetDataAsync("select top 1 shop_id as id, shop_name as name,shop_address1 as address,shop_city as city,shop_state as state,shop_zip as zip,shop_phone as phone from shops where shop_id=" + CType(e.Result.Nodes(0), System.Xml.Linq.XElement).Element("PortalShopID").Value.ToString, "Bubba985")
        Else
            If lOID.Content <> "" And lPID.Content <> "" Then
                bLink.Content = "<< Link >>"
                bLink.SetValue(Control.BackgroundProperty, New SolidColorBrush(Colors.Green))
                bLink.SetValue(Control.BorderBrushProperty, New SolidColorBrush(Colors.Green))
                bLink.Visibility = Windows.Visibility.Visible
            Else
                bLink.Visibility = Windows.Visibility.Collapsed
            End If
            Me.Cursor = Cursors.Arrow
        End If
    End Sub
    Private Sub P2GetDataCompleted(sender As Object, e As PortalData.GetDataCompletedEventArgs)
        Dim a As Linq.XElement
        Dim x As Linq.XElement
        Dim s As New List(Of CustomerRecord)
        a = e.Result.Nodes(1).Nodes(0)
        If Not a Is Nothing Then
            For y = 0 To a.Nodes.Count - 1
                x = CType(a.Nodes(y), System.Xml.Linq.XElement)
                s.Add(New CustomerRecord With {.Address = x.Element("address").Value, .ID = x.Element("id").Value, .Name = x.Element("name").Value, .City = x.Element("city").Value + ", " + x.Element("state").Value + " " + x.Element("zip").Value, .Phone = x.Element("phone").Value})
            Next
        End If
        If s.Count > 0 Then
            lPName.Content = s(0).Name
            lPAddress.Content = s(0).Address
            lPCSZ.Content = s(0).City + ", " + s(0).State + " " + s(0).Zip
            lPPhone.Content = s(0).Phone
            lPID.Content = s(0).ID
            bLink.Content = "Unlink"
            bLink.SetValue(Control.BackgroundProperty, New SolidColorBrush(Colors.Red))
            bLink.SetValue(Control.BorderBrushProperty, New SolidColorBrush(Colors.Red))
            bLink.Visibility = Windows.Visibility.Visible
        End If
        Me.Cursor = Cursors.Arrow
    End Sub

    Private Sub bSearchPortal_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles bSearchPortal.Click
        w = New cwFindShop(cwFindShop.eSearchType.Portal)
        w.Show()

    End Sub
    Public Class CustomerRecord
        Public Property ID
        Public Property Name
        Public Property Address
        Public Property City
        Public Property State
        Public Property Zip
        Public Property Phone
        Public Property ProgramID As String
        Public Property Startdate As Date
        Public Property Enddate As Date
    End Class
    Public Class ProgramRecord
        Public Property ID
        Public Property Program
    End Class
    Private Sub PopulatePrograms()
        Dim d As New ATIData.SvcSoapClient
        AddHandler d.XMLQueryCompleted, AddressOf PopulateProgramsQueryCompleted
        d.XMLQueryAsync("Select ID,Program from LearningPrograms", "Bubba985")
    End Sub
    Private Sub PopulateProgramsQueryCompleted(sender As Object, e As ATIData.XMLQueryCompletedEventArgs)
        Dim s As List(Of ProgramRecord) = (From x In e.Result.Elements Select New ProgramRecord With {.ID = x.Element("ID").Value, .Program = x.Element("Program").Value}).ToList
        s.Insert(0, New ProgramRecord With {.ID = "0", .Program = "None"})
        If s.Count > 0 Then
            cbPrograms.ItemsSource = s
            cbPrograms.DisplayMemberPath = "Program"
            cbPrograms.SelectedValuePath = "ID"
        End If
        Me.Cursor = Cursors.Arrow
    End Sub
    Private Sub LinK()
        Dim d As New ATIData.SvcSoapClient
        AddHandler d.XMLQueryCompleted, AddressOf LinkDone
        If bLink.Content = "Unlink" Then
            d.XMLQueryAsync("delete from PortalOETCustomerLinks where OETCustomerID=" + lOID.Content + " and PortalShopID=" + lPID.Content, "Bubba985")
        Else
            d.XMLQueryAsync("insert into PortalOETCustomerLinks (OETCustomerID,PortalShopID) values (" + lOID.Content + "," + lPID.Content + ")", "Bubba985")
        End If
    End Sub
    Private Sub SaveProgram()
        If lOID.Content = "" Then Exit Sub
        Me.IsEnabled = False
        Me.Cursor = Cursors.Wait
        Dim d As New ATIData.SvcSoapClient
        AddHandler d.XMLQueryCompleted, AddressOf SaveProgramDone
        d.XMLQueryAsync("update customers set LearningProgramID=" + cbPrograms.SelectedValue + ",LearningProgramStartDate='" + CDate(dpStart.SelectedDate).ToShortDateString + "',LearningProgramEndDate='" + CDate(dpEnd.SelectedDate).ToShortDateString + "' where id = " + lOID.Content, "Bubba985")
    End Sub
    Private Sub SaveProgramDone()
        Me.IsEnabled = True
        Me.Cursor = Cursors.Arrow
    End Sub
    Private Sub LinkDone()
        Clear()
    End Sub
    Private Sub Clear()
        lOName.Content = ""
        lOAddress.Content = ""
        lOCSZ.Content = ""
        lOPhone.Content = ""
        lOID.Content = ""
        lPName.Content = ""
        lPAddress.Content = ""
        lPCSZ.Content = ""
        lPPhone.Content = ""
        lPID.Content = ""
        bLink.Visibility = Windows.Visibility.Collapsed
        cbPrograms.SelectedIndex = 0
        dpStart.SelectedDate = Today
        dpStart.DisplayDate = Today
    End Sub

    Private Sub bClear_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles bClear.Click
        Clear()
    End Sub

    Private Sub bLink_Click(sender As Object, e As System.Windows.RoutedEventArgs) Handles bLink.Click
        LinK()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.Windows.RoutedEventArgs) Handles Button1.Click
        SaveProgram()
    End Sub
End Class
