﻿Partial Public Class ProgramManagement
    Inherits UserControl
    Dim WithEvents a As New ATIData.SvcSoapClient
    Dim s As List(Of ProgramRecord)
    Dim WithEvents w As cwAddEditProgram
    Dim WithEvents w2 As cwTest
    Public Type As String
    Public Sub New()
        InitializeComponent()
    End Sub
    Public Function Setup()
        Me.Cursor = Cursors.Wait
        If Type <> "T" Then
            lTestType.Visibility = Windows.Visibility.Collapsed
            cbTestTypes.Visibility = Windows.Visibility.Collapsed
            lTitle.Content = "Program Management"
            Button1.Content = "Add a Program"
            SayPrograms()
        Else
            lTestType.Visibility = Windows.Visibility.Visible
            cbTestTypes.Visibility = Windows.Visibility.Visible
            lTitle.Content = "Test Management"
            Button1.Content = "Add a Test"
            SayTests()
        End If
    End Function
    Private Sub SayPrograms()
        a.XMLQueryAsync("Select id,Program,CreditsRequired,Maximumcredits from LearningPrograms", "Bubba985")
    End Sub
    Private Sub SayTests()
        a.XMLQueryAsync("Select id,testname as Program from Tests where testtype='" + IIf(cbTestTypes.SelectedIndex = 0, "C", "T") + "'", "Bubba985")
    End Sub
    Public Class ProgramRecord
        Public Property ID As Integer
        Public Property Title As String
        Public Property CreditsRequired As String
        Public Property Total As String
    End Class
    Private Sub a_XMLQueryCompleted(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs) Handles a.XMLQueryCompleted
        s = (From x In e.Result.Elements Select New ProgramRecord With {.ID = x.Element("id").Value.ToString, .Title = x.Element("Program").Value, .CreditsRequired = x.Element("CreditsRequired"), .Total = x.Element("Maximumcredits")}).ToList
        DataGrid1.ItemsSource = s
        Me.Cursor = Cursors.Arrow
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles Button1.Click
        If Type <> "T" Then
            w = New cwAddEditProgram
            w.Setup()
            w.Show()
        Else
            w2 = New cwTest
            w2.Setup()
            w2.Show()
        End If
    End Sub
    Private Sub w_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles w.Closed
        SayPrograms()
    End Sub

    Private Sub Button_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs)
        If Type <> "T" Then
            w = New cwAddEditProgram
            w.ProgramID = CType(DataGrid1.SelectedItem, ProgramRecord).ID
            w.tTitle.Text = CType(DataGrid1.SelectedItem, ProgramRecord).Title
            w.tCreditsRequired.Text = CType(DataGrid1.SelectedItem, ProgramRecord).CreditsRequired
            w.tTotal.Text = CType(DataGrid1.SelectedItem, ProgramRecord).Total
            w.Setup()
            w.Show()
        Else
            w2 = New cwTest
            w2.ProgramID = CType(DataGrid1.SelectedItem, ProgramRecord).ID
            w2.tTitle.Text = CType(DataGrid1.SelectedItem, ProgramRecord).Title
            w2.Setup()
            w2.Show()
        End If
    End Sub

    Private Sub w2_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles w2.Closed
        SayTests()
    End Sub

    Private Sub cbTestTypes_SelectionChanged(ByVal sender As System.Object, ByVal e As System.Windows.Controls.SelectionChangedEventArgs) Handles cbTestTypes.SelectionChanged
        SayTests()
    End Sub
End Class