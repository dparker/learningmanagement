﻿Partial Public Class cwConfigureBonuses
    Inherits ChildWindow
    Dim s As List(Of BonusItem)

    Public Sub New()
        InitializeComponent()
    End Sub
    Public Sub Setup()
        GetItems()
    End Sub
    Private Sub OKButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs)

    End Sub

    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles CancelButton.Click
        Me.DialogResult = False
    End Sub

    Private Sub DataGrid1_CellEditEnded(ByVal sender As Object, ByVal e As System.Windows.Controls.DataGridCellEditEndedEventArgs) Handles DataGrid1.CellEditEnded
        bSave.Visibility = Windows.Visibility.Visible
    End Sub

    Private Sub DataGrid1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.Windows.Controls.SelectionChangedEventArgs) Handles DataGrid1.SelectionChanged

    End Sub
    Public Class BonusItem
        Public Sub New()

        End Sub
        Public Property ID As Integer
        Public Property Name As String
        Public Property Points As Integer
    End Class
    Private Sub GetItems()
        Dim q As New ATIData.SvcSoapClient
        AddHandler q.XMLQueryCompleted, AddressOf GetNewItemsHandler
        q.XMLQueryAsync("Select * from LearningProgramBonuses", "Bubba985")
    End Sub
    Private Sub GetNewItemsHandler(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs)
        s = (From x In e.Result.Elements Select New BonusItem With {.ID = x.Element("ID").Value.ToString, .Name = x.Element("BonusName").Value.ToString, .Points = x.Element("BonusPoints").Value}).ToList
        DataGrid1.ItemsSource = s
    End Sub
   
    Private Sub bSave_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles bSave.Click
        Dim x As Integer = 1
        Dim q As New ATIData.SvcSoapClient
        Dim TotalCredits As Integer = 0
        For Each r In s
            If r.ID > 0 Then
                q.DataNonQueryWithErrorAsync("Update LearningProgramBonuses set Bonusname='" + r.Name.ToString.Replace("'", "''") + "',BonusPoints=" + r.Points.ToString + " where id = " + r.ID.ToString, "Bubba985")
            Else
                q.DataNonQueryWithErrorAsync("Insert into LearningProgramBonuses (Bonusname,BonusPoints) values ('" + r.Name.ToString.Replace("'", "''") + "'," + r.Points.ToString + ")", "Bubba985")
            End If
        Next
        bSave.Visibility = Windows.Visibility.Collapsed
    End Sub

    Private Sub bAdd_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles bAdd.Click
        s.Add(New BonusItem With {.Name = "New Item"})
        DataGrid1.ItemsSource = Nothing
        DataGrid1.ItemsSource = s
        DataGrid1.UpdateLayout()
        DataGrid1.SelectedItem = s.ElementAt(s.Count - 1)
        DataGrid1.ScrollIntoView(s(s.Count - 1), DataGrid1.Columns(1))
    End Sub

   
End Class
