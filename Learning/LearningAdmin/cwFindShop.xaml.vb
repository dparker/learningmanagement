﻿Imports System.Xml

Partial Public Class cwFindShop
    Inherits ChildWindow
    Public _SearchType As eSearchType
    Dim WithEvents Cust As New ATIData.SvcSoapClient
    Dim WithEvents Cust2 As New PortalData.PortalDataSoapClient
    Dim sCustID As String
    Public ShopName As String = ""
    Public Address As String = ""
    Public CSZ As String = ""
    Public ShopID As String
    Public Phone As String
    Public ProgramID As String
    Public StartDate As Date
    Public EndDate As Date
    Public Sub New(SearchType As eSearchType)
        InitializeComponent()
        _SearchType = SearchType
    End Sub

    Private Sub OKButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles OKButton.Click
        If DataGrid1.SelectedItem Is Nothing Then
            MessageBox.Show("You must select a shop.", "Find Shop", MessageBoxButton.OK)
            Exit Sub
        Else
            Dim i As CustomerRecord
            i = DataGrid1.SelectedItem
            ShopName = i.Name
            Address = i.Address
            CSZ = i.City + ", " + i.State + " " + i.Zip
            ShopID = i.ID
            Phone = i.Phone
            ProgramID = i.ProgramID
            StartDate = i.StartDate
            EndDate = i.EndDate
        End If
        Me.DialogResult = True
    End Sub

    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles CancelButton.Click
        Me.DialogResult = False
    End Sub
    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.Windows.Controls.TextChangedEventArgs) Handles TextBox1.TextChanged
        If TextBox1.Text.Trim.Length > 2 Then
            If _SearchType = eSearchType.OET Then
                Cust.XMLQueryAsync("select top 50 id,name,address,city,state,zip,phone1 as phone,LearningProgramID,CONVERT(VARCHAR(10),LearningProgramStartDate,110) as StartDate,CONVERT(VARCHAR(10),LearningProgramEndDate,110) as EndDate from customers where name like '" + TextBox1.Text + "%'", "Bubba985")
            Else
                Cust2.GetDataAsync("select top 50 shop_id as id, shop_name as name,shop_address1 as address,shop_city as city,shop_state as state,shop_zip as zip,shop_phone as phone from shops where shop_name like '" + TextBox1.Text + "%'", "Bubba985")
            End If
        End If
    End Sub
    Private Sub Cust_XMLQueryCompleted(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs) Handles Cust.XMLQueryCompleted
        'Dim s As List(Of CustomerRecord) = (From x In e.Result.Elements Select New CustomerRecord With {.Address = x.Element("address").Value, .ID = x.Element("id").Value, .Name = x.Element("name").Value}).ToList
        Dim s As List(Of CustomerRecord) = (From x In e.Result.Elements Select New CustomerRecord With {.Address = x.Element("address").Value, .ID = x.Element("id").Value, .Name = x.Element("name").Value, .City = x.Element("city").Value + ", " + x.Element("state").Value + " " + x.Element("zip").Value, .Phone = x.Element("phone").Value, .ProgramID = x.Element("LearningProgramID"), .StartDate = x.Element("StartDate"), .EndDate = x.Element("EndDate")}).ToList
        DataGrid1.ItemsSource = s
        DataGrid1.Visibility = IIf(s.Count > 0, Visibility.Visible, Visibility.Collapsed)
    End Sub
    Public Class CustomerRecord
        Public Property ID
        Public Property Name
        Public Property Address
        Public Property City
        Public Property State
        Public Property Zip
        Public Property Phone
        Public Property ProgramID As String
        Public Property StartDate As Date
        Public Property EndDate As Date
    End Class

    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles TextBox1.GotFocus
        DataGrid1.Visibility = Visibility.Visible
    End Sub
    Public Enum eSearchType
        OET
        Portal
    End Enum

    Private Sub Cust2_GetDataCompleted(sender As Object, e As PortalData.GetDataCompletedEventArgs) Handles Cust2.GetDataCompleted
        Dim a As Linq.XElement
        Dim x As Linq.XElement
        Dim s As New List(Of CustomerRecord)
        a = e.Result.Nodes(1).Nodes(0)
        If Not a Is Nothing Then
            For y = 0 To a.Nodes.Count - 1
                x = CType(a.Nodes(y), System.Xml.Linq.XElement)
                s.Add(New CustomerRecord With {.Address = x.Element("address").Value, .ID = x.Element("id").Value, .Name = x.Element("name").Value, .City = x.Element("city").Value + ", " + x.Element("state").Value + " " + x.Element("zip").Value, .Phone = x.Element("phone").Value})
            Next
        End If
        'a = a
        'Dim s As List(Of CustomerRecord) = (From x In a Select New CustomerRecord With {.Address = x.Element("address").Value, .ID = x.Element("id").Value, .Name = x.Element("name").Value, .City = x.Element("city").Value + ", " + x.Element("state").Value + " " + x.Element("zip").Value}).ToList
        DataGrid1.ItemsSource = s
        DataGrid1.Visibility = IIf(s.Count > 0, Visibility.Visible, Visibility.Collapsed)
    End Sub
End Class
