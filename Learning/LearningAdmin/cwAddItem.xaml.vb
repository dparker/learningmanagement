﻿Partial Public Class cwAddItem
    Inherits ChildWindow
    Public ProgramID As String
    Public Sub New()
        InitializeComponent()
        GetData()
    End Sub

    Private Sub OKButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles OKButton.Click
        Dim i As ItemRecord
        Dim q As New ATIData.SvcSoapClient
        Dim sType As String = ""
        If ComboBox1.SelectedIndex = 0 Then
            sType = "C"
        ElseIf ComboBox1.SelectedIndex = 1 Then
            sType = "T"
        ElseIf ComboBox1.SelectedIndex = 2 Then
            sType = "S"
        End If

        For Each i In DataGrid1.ItemsSource
            If i.Include Then
                q.DataNonQueryAsync("Insert into LearningProgramItems (ProgramID,ItemID,ItemType,Credits) values (" + ProgramID + "," + i.ID.ToString + ",'" + sType + "'," + Val(i.Credits.ToString).ToString + ")", "Bubba985")
            End If
        Next
        Me.DialogResult = True
    End Sub

    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles CancelButton.Click
        Me.DialogResult = False
    End Sub
    Private Sub GetData()
        Dim q As New ATIData.SvcSoapClient
        AddHandler q.XMLQueryCompleted, AddressOf DataCompleted
        If ComboBox1.SelectedIndex = 0 Then
            q.XMLQueryAsync("select id,seminartitle from courses where class=1 order by seminartitle", "Bubba985")
        ElseIf ComboBox1.SelectedIndex = 1 Then
            q.XMLQueryAsync("select id,TestName as seminartitle from Tests where TestType='C' order by seminartitle", "Bubba985")
        ElseIf ComboBox1.SelectedIndex = 2 Then
            q.XMLQueryAsync("select id,TestName as seminartitle from Tests where TestType='T' order by seminartitle", "Bubba985")
        End If
    End Sub
    Private Sub DataCompleted(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs)
        Dim s As List(Of ItemRecord) = (From x In e.Result.Elements Select New ItemRecord With {.ID = x.Element("id").Value.ToString, .Title = x.Element("seminartitle").Value, .Credits = ""}).ToList
        DataGrid1.ItemsSource = s
    End Sub

    Public Class ItemRecord
        Public Property ID As Integer
        Public Property Include As Boolean
        Public Property Title As String
        Public Property Credits As String
        Public Sub New()
            Include = False
        End Sub
    End Class

   
    Private Sub ComboBox1_SelectionChanged(ByVal sender As System.Object, ByVal e As System.Windows.Controls.SelectionChangedEventArgs) Handles ComboBox1.SelectionChanged
        GetData()
    End Sub
End Class
