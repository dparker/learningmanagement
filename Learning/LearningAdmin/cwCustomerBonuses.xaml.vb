﻿Partial Public Class cwCustomerBonuses
    Inherits ChildWindow
    Dim WithEvents Cust As New ATIData.SvcSoapClient
    Dim sCustID As String
    Dim b As List(Of Bonus)
    Dim WithEvents a As cwAddBonus
    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub OKButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles OKButton.Click
        Me.DialogResult = True
    End Sub

    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles CancelButton.Click
        Me.DialogResult = False
    End Sub
    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.Windows.Controls.TextChangedEventArgs) Handles TextBox1.TextChanged
        If TextBox1.Text.Trim.Length > 2 Then
            Cust.XMLQueryAsync("select top 50 id,name,address,city,state,zip from customers where name like '" + TextBox1.Text + "%'", "Bubba985")
        End If
    End Sub
    Private Sub Cust_XMLQueryCompleted(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs) Handles Cust.XMLQueryCompleted
        'Dim s As List(Of CustomerRecord) = (From x In e.Result.Elements Select New CustomerRecord With {.Address = x.Element("address").Value, .ID = x.Element("id").Value, .Name = x.Element("name").Value}).ToList
        Dim s As List(Of CustomerRecord) = (From x In e.Result.Elements Select New CustomerRecord With {.Address = x.Element("address").Value, .ID = x.Element("id").Value, .Name = x.Element("name").Value, .City = x.Element("city").Value + ", " + x.Element("state").Value + " " + x.Element("zip").Value}).ToList

        DataGrid1.ItemsSource = s
        DataGrid1.Visibility = IIf(s.Count > 0, Visibility.Visible, Visibility.Collapsed)
    End Sub
    Public Class CustomerRecord
        Public Property ID
        Public Property Name
        Public Property Address
        Public Property City
        Public Property State
        Public Property Zip
        Public Property Phone
    End Class

    Private Sub TextBox1_GotFocus(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles TextBox1.GotFocus
        DataGrid1.Visibility = Visibility.Visible
    End Sub

    Private Sub TextBox1_LostFocus(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles TextBox1.LostFocus
        DataGrid1.Visibility = Visibility.Collapsed
    End Sub
    Private Sub DataGrid1_SelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Controls.SelectionChangedEventArgs) Handles DataGrid1.SelectionChanged
        If Not DataGrid1.SelectedItem Is Nothing Then
            Dim q As New ATIData.SvcSoapClient
            AddHandler q.XMLQueryCompleted, AddressOf Me.SayCustomer
            q.XMLQueryAsync("Select id,name,address,city,state,zip,phone1 from customers where id=" + CType(DataGrid1.SelectedItem, CustomerRecord).ID.ToString, "Bubba985")
            GetBonuses()
        End If
    End Sub
    Private Sub GetBonuses()
        Dim q2 As New ATIData.SvcSoapClient
        AddHandler q2.XMLQueryCompleted, AddressOf Me.SayBonsuses
        q2.XMLQueryAsync("Select learningcustomerbonuses.id,learningprogrambonuses.bonusname,learningcustomerbonuses.points from learningcustomerbonuses left join learningprogrambonuses on learningcustomerbonuses.bonusid=learningprogrambonuses.id where customerid=" + CType(DataGrid1.SelectedItem, CustomerRecord).ID.ToString, "Bubba985")

    End Sub
    Private Sub SayCustomer(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs)
        Dim s As List(Of CustomerRecord) = (From x In e.Result.Elements Select New CustomerRecord With {.Address = x.Element("address").Value, .ID = x.Element("id").Value, .Name = x.Element("name").Value, .State = x.Element("state").Value, .City = x.Element("city").Value, .Zip = x.Element("zip").Value, .Phone = x.Element("phone1").Value}).ToList
        Label2.Content = s(0).Name + vbCrLf + s(0).Address + vbCrLf + s(0).City + ", " + s(0).State + " " + s(0).Zip + vbCrLf + s(0).Phone
        sCustID = s(0).ID.ToString
        DataGrid1.Visibility = Visibility.Collapsed

    End Sub
    Private Sub SayBonsuses(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs)
        b = (From x In e.Result.Elements Select New Bonus With {.ID = x.Element("id").Value, .Bonus = x.Element("bonusname").Value, .Points = x.Element("points").Value}).ToList
        DataGrid2.ItemsSource = b
        DataGrid2.Visibility = IIf(b.Count > 0, Visibility.Visible, Visibility.Collapsed)
        bAdd.Visibility = Visibility.Visible
    End Sub
    Public Class Bonus
        Public Property ID As Integer
        Public Property Bonus As String
        Public Property Points As Double
    End Class

    Private Sub bAdd_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles bAdd.Click
        a = New cwAddBonus
        a.sCustID = sCustID
        a.Show()
    End Sub

    Private Sub a_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles a.Closed
        GetBonuses()
    End Sub
    Private Sub Button_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs)
        Dim q As New ATIData.SvcSoapClient
        AddHandler q.DataNonQueryCompleted, AddressOf DeleteDone
        q.DataNonQueryAsync("delete from LearningCustomerBonuses where id = " + CType(DataGrid2.SelectedItem, Bonus).ID.ToString, "Bubba985")
    End Sub
    Private Sub DeleteDone()
        GetBonuses()
    End Sub
End Class
