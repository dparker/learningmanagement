﻿Partial Public Class cwAddEditProgram
    Inherits ChildWindow
    Public ProgramID As String = ""
    Dim WithEvents w As cwAddItem
    Dim s As List(Of ItemRecord)
    Public Sub New()
        InitializeComponent()
    End Sub
    Public Sub Setup()
        If ProgramID = "" Then
            Button1.Content = "Save"
            DataGrid1.IsEnabled = False
            bUp.IsEnabled = False
            bDown.IsEnabled = False
            OKButton.IsEnabled = False
        Else
            Button1.Content = "Edit"
            tTitle.IsEnabled = False
            tCreditsRequired.IsEnabled = False
            tTotal.IsEnabled = False
            GetItems()
        End If
    End Sub
    Private Sub OKButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles OKButton.Click
        'Me.DialogResult = True
        w = New cwAddItem
        w.ProgramID = ProgramID
        w.Show()
    End Sub
    Private Sub GetItems()
        Dim q As New ATIData.SvcSoapClient
        AddHandler q.XMLQueryCompleted, AddressOf GetNewItemsHandler
        q.XMLQueryAsync("Select learningprogramitems.ID,ISNULL(courses.seminartitle,tests.testname) as seminartitle,Credits,ItemType=CASE ItemType WHEN 'C' THEN 'Class' WHEN 'T' THEN 'Class Test' WHEN 'S' THEN 'Teleseminar' ELSE '?' END from learningprogramitems left join courses on learningprogramitems.itemid=courses.id left join Tests on LearningProgramItems.ItemID = tests.ID where programid=" + ProgramID + " order by priority", "Bubba985")
    End Sub
    Private Sub GetNewItemsHandler(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs)
        Dim TotalCredits As Integer
        s = (From x In e.Result.Elements Select New ItemRecord With {.ID = x.Element("ID").Value.ToString, .Title = x.Element("ItemType").Value.ToString + " : " + x.Element("seminartitle").Value.ToString, .Credits = x.Element("Credits").Value}).ToList
        For Each a In e.Result.Elements
            TotalCredits = TotalCredits + a.Element("Credits").Value
        Next
        lCredits.Content = TotalCredits
        Dim y As Integer = 0
        For Each i In s
            i.Priority = y
            y = y + 1
        Next
        DataGrid1.ItemsSource = s
    End Sub
    Class ItemRecord
        Property ID As String
        Property Title As String
        Property Priority As Integer
        Property Credits As Integer
    End Class
    Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As RoutedEventArgs) Handles CancelButton.Click
        Me.DialogResult = False
    End Sub

    Private Sub Button1_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles Button1.Click
        If ProgramID = "" And Button1.Content = "Save" Then
            Me.Cursor = Cursors.Wait
            Dim q As New ATIData.SvcSoapClient
            AddHandler q.XMLQueryCompleted, AddressOf GetNewID
            q.XMLQueryAsync("Insert into LearningPrograms (Program,CreditsRequired,MaximumCredits) values ('" + tTitle.Text.Replace("'", "''") + "'," + Val(tCreditsRequired.Text).ToString + "," + Val(tTotal.Text).ToString + ") select ID from LearningPrograms where ID = @@Identity", "Bubba985")
            tTitle.IsEnabled = False
            tCreditsRequired.IsEnabled = False
            tTotal.IsEnabled = False
            DataGrid1.IsEnabled = True
            OKButton.IsEnabled = True
            bSave.IsEnabled = True
            bUp.IsEnabled = True
            bDown.IsEnabled = True
            Button1.Content = "Edit"
        ElseIf ProgramID <> "" And Button1.Content = "Save" Then
            Dim q As New ATIData.SvcSoapClient
            q.DataNonQueryAsync("Update LearningPrograms set Program='" + tTitle.Text.Replace("'", "''") + "',CreditsRequired=" + tCreditsRequired.Text + ",MaximumCredits=" + tTotal.Text + " where id=" + ProgramID, "Bubba985")
            tTitle.IsEnabled = False
            tCreditsRequired.IsEnabled = False
            tTotal.IsEnabled = False
            DataGrid1.IsEnabled = True
            OKButton.IsEnabled = True
            bSave.IsEnabled = True
            bUp.IsEnabled = True
            bDown.IsEnabled = True
            Button1.Content = "Edit"
        Else
            Button1.Content = "Save"
            tTitle.IsEnabled = True
            tCreditsRequired.IsEnabled = True
            tTotal.IsEnabled = True
            DataGrid1.IsEnabled = False
            OKButton.IsEnabled = False
            bSave.IsEnabled = False
            bUp.IsEnabled = False
            bDown.IsEnabled = False
        End If
    End Sub
    Private Sub GetNewID(ByVal sender As Object, ByVal e As ATIData.XMLQueryCompletedEventArgs)
        ProgramID = e.Result.Value.ToString
        Me.Cursor = Cursors.Arrow
        Button1.Content = "Edit"
        DataGrid1.IsEnabled = True
        bUp.IsEnabled = True
        bDown.IsEnabled = True
        OKButton.IsEnabled = True
    End Sub

    Private Sub w_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles w.Closed
        GetItems()
    End Sub

    Private Sub Button_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs)
        Dim q As New ATIData.SvcSoapClient
        q.DataNonQueryAsync("delete from LearningProgramitems where id = " + CType(DataGrid1.SelectedItem, ItemRecord).ID, "Bubba985")
        GetItems()
    End Sub

   
    Private Sub bDown_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles bDown.Click
        If DataGrid1.SelectedIndex >= s.Count - 1 Then Exit Sub
        Dim r As ItemRecord = DataGrid1.SelectedItem
        s.RemoveAt(DataGrid1.SelectedIndex)
        s.Insert(DataGrid1.SelectedIndex + 1, r)
        DataGrid1.ItemsSource = s
        DataGrid1.SelectedIndex = DataGrid1.SelectedIndex + 1
        bSave.Visibility = Windows.Visibility.Visible
    End Sub
    Public Class ItemCompare
        Implements IComparer(Of ItemRecord)
        Public Function Compare(ByVal x As ItemRecord, ByVal y As ItemRecord) As Integer Implements System.Collections.Generic.IComparer(Of ItemRecord).Compare
            Return x.Priority.CompareTo(x.Priority)
        End Function
    End Class

    Private Sub bUp_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles bUp.Click
        If DataGrid1.SelectedIndex = 0 Then Exit Sub
        Dim r As ItemRecord = DataGrid1.SelectedItem
        s.RemoveAt(DataGrid1.SelectedIndex)
        s.Insert(DataGrid1.SelectedIndex - 1, r)
        DataGrid1.ItemsSource = s
        DataGrid1.SelectedIndex = DataGrid1.SelectedIndex - 1
        bSave.Visibility = Windows.Visibility.Visible
    End Sub

    Private Sub bSave_Click(ByVal sender As Object, ByVal e As System.Windows.RoutedEventArgs) Handles bSave.Click
        Dim x As Integer = 1
        Dim q As New ATIData.SvcSoapClient
        Dim TotalCredits As Integer = 0
        For Each r In s
            q.DataNonQueryWithErrorAsync("Update LearningProgramItems set [Priority]=" + x.ToString + ",Credits=" + r.Credits.ToString + " where id = " + r.ID, "Bubba985")
            TotalCredits = TotalCredits + r.Credits
            x = x + 1
        Next
        lCredits.Content = TotalCredits.ToString
        bSave.Visibility = Windows.Visibility.Collapsed
    End Sub

    Private Sub DataGrid1_CellEditEnded(ByVal sender As Object, ByVal e As System.Windows.Controls.DataGridCellEditEndedEventArgs) Handles DataGrid1.CellEditEnded
        bSave.Visibility = Windows.Visibility.Visible
    End Sub

End Class
