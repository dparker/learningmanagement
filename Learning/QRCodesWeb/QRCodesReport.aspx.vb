﻿Public Class QRCodesReport
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then GetData()
    End Sub
    Private Sub GetData()
        Dim ds As DataSet
        ds = GeneralDataSet("Select QRScans.EventCode as Event,QRScans.ScanDate as [Scan Date],people.name as Contact,customers.Name,customers.Address,customers.City,customers.State,customers.Zip,customers.Phone1 as Phone,QRScans.note as Note from QRScans left join People on QRScans.PeopleID=People.ID left join customers on people.customerid=customers.id")
        UltraWebGrid1.DataSource = ds
        UltraWebGrid1.DataBind()

    End Sub

End Class