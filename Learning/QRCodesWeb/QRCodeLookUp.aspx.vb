﻿Public Class QRCodeLookUp
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then GetData()
    End Sub
    Private Sub GetData()
        Dim ds As DataSet = GeneralDataSet("Select customers.name,customers.address,customers.city,customers.state,customers.zip,customers.phone1,people.name as Contact from People left join customers on people.customerid=customers.id where people.id=" + Request.QueryString("code").ToString)
        If Not ds.Tables(0).Rows.Count = 0 Then
            lContact.Text = ds.Tables(0).Rows(0).Item("contact").ToString
            lName.Text = ds.Tables(0).Rows(0).Item("name").ToString
            lAddress.Text = ds.Tables(0).Rows(0).Item("address").ToString
            lCSZ.Text = ds.Tables(0).Rows(0).Item("city").ToString.Trim + ", " + ds.Tables(0).Rows(0).Item("state").ToString.Trim + " " + ds.Tables(0).Rows(0).Item("zip").ToString.Trim
            lPhone.Text = ds.Tables(0).Rows(0).Item("phone1").ToString
        End If
    End Sub

    Protected Sub bSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles bSave.Click
        GeneralQuery("Insert into QRScans (Peopleid,eventcode,scandate,note) values (" + Request.QueryString("code").ToString + ",'" + tCode.Text.ToString + "','" + Now.ToShortDateString + " " + Now.ToShortTimeString + "','" + tNotes.Text + "')")
        lName.Text = "Saved"
        lAddress.Visible = False
        lCSZ.Visible = False
        lPhone.Visible = False
        tCode.Visible = False
        tNotes.Visible = False
        bSave.Visible = False
        Label1.Visible = False
        Label2.Visible = False
    End Sub
End Class