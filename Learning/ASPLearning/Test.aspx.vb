﻿Public Class Test
    Inherits System.Web.UI.Page
    Private Shared prevPage As String = String.Empty
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Try
                prevPage = Request.UrlReferrer.ToString()
            Catch
                prevPage = "http://www.autotraining.net"
            End Try
            GetTest()
        End If
    End Sub
    Private Sub GetTest()
        Dim ds As DataSet
        Dim ds2 As DataSet
        'Dim sXML As String = "<Test><Questions>"
        Dim l As Label
        Dim tb As Table
        Dim tr As TableRow
        Dim tc As TableCell
        lGrade.Text = ""
        '        lTitle.Text = GeneralData("select top 1 testname from tests where id=" + Request.QueryString("TestID").ToString + " order by id")
        '       ds = GeneralDataSet("Select id,'' as seq, question from testquestions where testid=" + Request.QueryString("TestID").ToString)
        lTitle.Text = GeneralData("select top 1 testname from tests where id=1 order by id")
        ds = GeneralDataSet("Select id,'' as seq, question from testquestions where testid=1")
        Dim r As New Random
        Dim x As Integer
        For x = 0 To ds.Tables(0).Rows.Count - 1
            ds.Tables(0).Rows(x).Item("seq") = Math.Round((100 * r.NextDouble), 0).ToString
            '   sXML = sXML + "<Q" + x.ToString + ">" + ds.Tables(0).Rows(x).Item("seq").ToString + "</Q" + x.ToString + ">"
        Next
        'sXML = sXML + "</Questions><Answers>"
        hQuestionCount.Value = ds.Tables(0).Rows.Count.ToString
        Dim dv As New DataView
        dv.Table = ds.Tables(0)
        dv.Sort = "seq"
        WebTab1.Tabs.Clear()
        For x = 0 To dv.Table.Rows.Count - 1
            tb = New Table
            ds2 = GeneralDataSet("select id,'' as seq,answer,correct from testanswers where questionid=" + dv.Item(x).Item("id").ToString)
            r = New Random
            For y = 0 To ds2.Tables(0).Rows.Count - 1
                ds2.Tables(0).Rows(y).Item("seq") = Math.Round((100 * r.NextDouble), 0).ToString
                '       sXML = sXML + "<Q" + x.ToString + "A" + y.ToString + ">" + ds2.Tables(0).Rows(y).Item("seq").ToString + "</Q" + x.ToString + "A" + y.ToString + ">"
            Next
            Dim dv2 As New DataView
            dv2.Table = ds2.Tables(0)
            dv2.Sort = "seq"

            Dim t As New Infragistics.Web.UI.LayoutControls.ContentTabItem
            t.Text = ""
            t.BackColor = Drawing.Color.Bisque

            l = New Label
            l.Font.Size = 18
            l.Text = "Correct"
            l.ID = "Correct" + x.ToString
            l.Style.Add("margin", "10px")
            l.Style.Add("Visibility", "hidden")
            tr = New TableRow
            tr.Attributes.Add("Height", "45")
            tc = New TableCell
            tc.Controls.Add(l)
            tr.Cells.Add(tc)
            tb.Rows.Add(tr)

            l = New Label
            l.Font.Size = 18
            l.Text = dv.Item(x).Item("question")
            l.ID = "Question" + dv.Item(x).Item("ID").ToString
            l.Style.Add("margin", "10px")
            tr = New TableRow
            tr.Attributes.Add("Height", "45")
            tc = New TableCell
            tc.Controls.Add(l)
            tr.Cells.Add(tc)
            tb.Rows.Add(tr)
            't.Controls.Add(l)

            For y = 0 To dv2.Table.Rows.Count - 1
                Dim rb As New RadioButton
                rb.Font.Size = 12
                rb.GroupName = x.ToString
                rb.Text = dv2.Item(y).Item("answer").ToString
                rb.ID = "Answer" + dv2.Item(y).Item("ID").ToString
                rb.Attributes.Add("onclick", "return CheckAnswer(this," + IIf(dv2.Item(y).Item("correct").ToString.ToUpper = "TRUE", "1", "0") + ",'WebTab1_Correct" + x.ToString + "','" + dv2.Item(y).Item("ID").ToString + "','" + dv.Item(x).Item("ID").ToString + "')")
                rb.Style.Add("margin-left", "50px")
                rb.Style.Add("[disabled] color", "red")
                tr = New TableRow
                tr.Attributes.Add("Height", "30")
                tc = New TableCell
                tc.Controls.Add(rb)
                tr.Cells.Add(tc)
                tb.Rows.Add(tr)
                '                t.Controls.Add(rb)
            Next
            t.Controls.Add(tb)
            WebTab1.Tabs.Add(t)
        Next
        'sXML = sXML + "</Answers></Test>"
        'sXML = sXML.Replace("<", "{")
        'sXML = sXML.Replace(">", "}")
        hTestXML.Value = ""
        lCounter.Text = "Question 1 of " + dv.Table.Rows.Count.ToString
        WebTab1.SelectedIndex = 0
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles bGrade.Click
        Dim Grade As Integer
        Dim QuestionCount As Integer = Val(hQuestionCount.Value)
        Dim CorrectCount As Integer = Val(hCorrectCount.Value)
        Dim RetakeCount As Integer = 0
        Dim sID As String = ""
        Dim sXMLTemp As String = hTestXML.Value
        Dim sXML = "<TestAnswers>" + hTestXML.Value.Replace("{", "<").Replace("}", ">") + "</TestAnswers>"
        If QuestionCount > 0 Then
            Grade = Math.Round(CorrectCount / QuestionCount * 100, 0)
        Else
            Grade = 0
        End If
        Dim ds As DataSet = GeneralDataSet("Select ID,retakecount from testresults where customerid=" + Request.QueryString("CustomerID") + " and testid=" + Request.QueryString("TestID"))
        If ds.Tables(0).Rows.Count > 0 Then
            RetakeCount = ds.Tables(0).Rows(0).Item("Retakecount") + 1
            sID = ds.Tables(0).Rows(0).Item("ID")
        End If
        If sID = "" Then
            GeneralQuery("INSERT INTO [TestResults] ([CustomerID],[TestID],[Grade],[TestAnswers],[RetakeCount],[DateTaken]) VALUES(" + Request.QueryString("CustomerID") + "," + Request.QueryString("TestID") + "," + Grade.ToString + ",'" + sXML + "',0,'" + Today.ToShortDateString + "')")
        Else
            GeneralQuery("update testresults set grade=" + Grade.ToString + ",testanswers='" + sXML + "',retakecount=" + RetakeCount.ToString + ",DateTaken='" + Today.ToShortDateString + "' where customerid=" + Request.QueryString("CustomerID") + " and testid=" + Request.QueryString("testid"))
        End If
        Response.Redirect(prevPage)
    End Sub

    Private Sub bCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bCancel.Click
        Response.Redirect(prevPage)
    End Sub

End Class