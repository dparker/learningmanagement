﻿Public Class LearningDetails
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Not IsPostBack Then
        GetData()
        'End If
    End Sub
    Private Sub GetData()
        Dim ds As DataSet
        Dim ProgressMonths As Integer = 0
        Dim TotalMonths As Integer = 0
        Dim TotalProgress As Integer = 0
        ds = GeneralDataSet("EXEC LearningTally @LearningProgramID = 13,@CustomerID = 426")
        If ds.Tables.Count > 1 Then
            If ds.Tables(1).Rows.Count > 0 Then
                ProgressMonths = Val(ds.Tables(1).Rows(0).Item("ProgressMonths").ToString)
                TotalMonths = Val(ds.Tables(1).Rows(0).Item("TotalMonths").ToString)
                lStart.Text = ds.Tables(1).Rows(0).Item("StartDate").ToString
                lEnd.Text = ds.Tables(1).Rows(0).Item("EndDate").ToString
            End If
        End If
        If TotalMonths > 0 Then
            TotalProgress = Math.Round(100 * ProgressMonths / TotalMonths, 0)
        End If
        lMonths.Text = ProgressMonths.ToString + " of " + TotalMonths.ToString + " months (" + TotalProgress.ToString + "%)"
        pProgram.Attributes.Add("style", "width:" + TotalProgress.ToString + "%;top:52px;position:absolute")

        For x = 0 To ds.Tables(1).Rows.Count - 1

        Next
    End Sub

End Class