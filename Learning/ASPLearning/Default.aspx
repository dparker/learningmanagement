﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="ASPLearning._Default" %>

<%@ Register Assembly="Infragistics4.WebUI.UltraWebGauge.v11.1, Version=11.1.20111.1006, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.WebUI.UltraWebGauge" tagprefix="igGauge" %>
<%@ Register Assembly="Infragistics4.WebUI.UltraWebGauge.v11.1, Version=11.1.20111.1006, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.UltraGauge.Resources" tagprefix="igGaugeProp" %>

<%@ Register Assembly="Infragistics4.WebUI.WebResizingExtender.v11.1, Version=11.1.20111.1006, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.WebUI" tagprefix="igui" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #form1
        {
            height: 690px;
            width: 1253px;
        }
        </style>
    <script type="text/javascript" id="igClientScript">
<!--

        function GaugeClick(oGauge, oEvent, x, y) {
            var ShopID;
            var mywindow;
            ShopID = document.getElementById('hShopID').value;
            // window.navigate('LearningDetails1.aspx?PortalShopID='+ShopID);        
            mywindow = window.open('LearningDetails1.aspx?PortalShopID=' + ShopID, 'ATI', 'location=no,menubar=no,resizable=yes,status=no,titlebar=no,toolbar=no,scrollbars=yes,height=800,width=850');
            mywindow.moveTo(10, 10);  

          }
// -->
</script>
</head>
<!-- <body bgcolor="#3366ff" style="background-image:Images/ATIShroudGaugeTransparentArt122811.jpg" />-->
<body onclick="GaugeClick()" />
    <form id="form1" runat="server">
    <img src="Images/ATIShroudGaugeCropped.png" />
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Small"  
        ForeColor="White" Text="ATI Learning Management System<br/>Click for Details" 
        
        style="z-index: 1; left: 319px; top: 261px; position: absolute; height: 31px; width: 201px; text-align:center"></asp:Label>
    <div>
    
    </div>
    <igGauge:UltraGauge ID="gLearning" runat="server" BackColor="Transparent" 
        ForeColor="ControlLightLight" Height="181px" 
        style="z-index: 1; left: 231px; top: 58px; position: absolute; height: 181px; width: 216px" 
        Width="216px">
        <Gauges>
            <igGaugeProp:RadialGauge MarginString="2, 2, 2, 2, Pixels">
                <Dial>
                    <BrushElements>
                        <igGaugeProp:BrushElementGroup>
                            <BrushElements>
                                <igGaugeProp:MultiStopRadialGradientBrushElement CenterPointString="50, 50" 
                                    FocusScalesString="0.8, 0.8">
                                    <ColorStops>
                                        <igGaugeProp:ColorStop Color="161, 161, 161" />
                                        <igGaugeProp:ColorStop Color="96, 96, 96" Stop="0.4758621" />
                                        <igGaugeProp:ColorStop Color="89, 89, 89" Stop="1" />
                                    </ColorStops>
                                </igGaugeProp:MultiStopRadialGradientBrushElement>
                                <igGaugeProp:SolidFillBrushElement Color="Gray" RelativeBounds="4, 4, 93, 93" 
                                    RelativeBoundsMeasure="Percent" />
                                <igGaugeProp:MultiStopRadialGradientBrushElement CenterPointString="50, 50" 
                                    RelativeBounds="4, 4, 93, 93" RelativeBoundsMeasure="Percent">
                                    <ColorStops>
                                        <igGaugeProp:ColorStop Color="Transparent" />
                                        <igGaugeProp:ColorStop Color="141, 141, 141" Stop="0.06206897" />
                                        <igGaugeProp:ColorStop Color="82, 82, 82" Stop="0.07586207" />
                                        <igGaugeProp:ColorStop Color="White" Stop="0.1448276" />
                                        <igGaugeProp:ColorStop Color="White" Stop="0.2448276" />
                                        <igGaugeProp:ColorStop Color="White" Stop="1" />
                                    </ColorStops>
                                </igGaugeProp:MultiStopRadialGradientBrushElement>
                            </BrushElements>
                        </igGaugeProp:BrushElementGroup>
                    </BrushElements>
                    <StrokeElement>
                        <BrushElements>
                            <igGaugeProp:SolidFillBrushElement Color="Silver" />
                        </BrushElements>
                    </StrokeElement>
                </Dial>
                <Scales>
                    <igGaugeProp:RadialGaugeScale EndAngle="396" StartAngle="144">
                        <MajorTickmarks EndExtent="79" EndWidth="3" Frequency="10" StartExtent="67" 
                            StartWidth="3">
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="189, 189, 189" />
                            </BrushElements>
                        </MajorTickmarks>
                        <MinorTickmarks EndExtent="78" EndWidth="1" Frequency="2" StartExtent="73">
                            <StrokeElement>
                                <BrushElements>
                                    <igGaugeProp:SolidFillBrushElement Color="135, 135, 135" />
                                </BrushElements>
                            </StrokeElement>
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="240, 240, 240" />
                            </BrushElements>
                        </MinorTickmarks>
                        <Labels Extent="55" Font="Arial, 12px, style=Bold" Frequency="20" 
                            Orientation="Horizontal" SpanMaximum="18">
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="Black" />
                            </BrushElements>
                        </Labels>
                        <Markers>
                            <igGaugeProp:RadialGaugeNeedle EndExtent="65" EndWidth="1" MidExtent="0" 
                                MidWidth="3" StartExtent="-20" StartWidth="3" ValueString="25" 
                                WidthMeasure="Percent">
                                <Anchor RadiusMeasure="Percent">
                                    <BrushElements>
                                        <igGaugeProp:SimpleGradientBrushElement EndColor="64, 64, 64" 
                                            GradientStyle="BackwardDiagonal" StartColor="Gainsboro" />
                                    </BrushElements>
                                    <StrokeElement Thickness="2">
                                        <BrushElements>
                                            <igGaugeProp:RadialGradientBrushElement CenterColor="WhiteSmoke" 
                                                SurroundColor="Gray" />
                                        </BrushElements>
                                    </StrokeElement>
                                </Anchor>
                                <StrokeElement Thickness="0">
                                </StrokeElement>
                                <BrushElements>
                                    <igGaugeProp:SolidFillBrushElement Color="255, 61, 22" />
                                </BrushElements>
                            </igGaugeProp:RadialGaugeNeedle>
                            <igGaugeProp:RadialGaugeNeedle EndWidth="9" MidExtent="80" MidWidth="0" 
                                StartExtent="78" StartWidth="0" ValueString="50">
                                <BrushElements>
                                    <igGaugeProp:SolidFillBrushElement Color="38, 232, 0" />
                                </BrushElements>
                            </igGaugeProp:RadialGaugeNeedle>
                        </Markers>
                        <Axes>
                            <igGaugeProp:NumericAxis EndValue="1000" TickmarkInterval="10" />
                        </Axes>
                    </igGaugeProp:RadialGaugeScale>
                </Scales>
            </igGaugeProp:RadialGauge>
        </Gauges>
        <Annotations>
            <igGaugeProp:BoxAnnotation Bounds="69, 130, 82, 22">
                <Label Font="Arial, 10pt, style=Bold" FormatString="Credits (X10)">
                    <BrushElements>
                        <igGaugeProp:SolidFillBrushElement Color="White" />
                    </BrushElements>
                </Label>
                <BrushElements>
                    <igGaugeProp:SolidFillBrushElement Color="Silver" />
                </BrushElements>
            </igGaugeProp:BoxAnnotation>
        </Annotations>
        <DeploymentScenario FilePath="" ImageURL="#CLIENT_#SESSION.#EXT" />
    </igGauge:UltraGauge>
    <igGauge:UltraGauge ID="gMonths" runat="server" BackColor="Transparent" 
        ForeColor="ControlLightLight" Height="181px" 
        style="z-index: 1; left: 47px; top: 76px; position: absolute; height: 181px; width: 216px; right: 847px;" 
        Width="216px">
        <Gauges>
            <igGaugeProp:RadialGauge MarginString="2, 2, 2, 2, Pixels">
                <Dial>
                    <BrushElements>
                        <igGaugeProp:BrushElementGroup>
                            <BrushElements>
                                <igGaugeProp:MultiStopRadialGradientBrushElement CenterPointString="50, 50" 
                                    FocusScalesString="0.8, 0.8">
                                    <ColorStops>
                                        <igGaugeProp:ColorStop Color="161, 161, 161" />
                                        <igGaugeProp:ColorStop Color="96, 96, 96" Stop="0.4758621" />
                                        <igGaugeProp:ColorStop Color="89, 89, 89" Stop="1" />
                                    </ColorStops>
                                </igGaugeProp:MultiStopRadialGradientBrushElement>
                                <igGaugeProp:SolidFillBrushElement Color="Gray" RelativeBounds="4, 4, 93, 93" 
                                    RelativeBoundsMeasure="Percent" />
                                <igGaugeProp:MultiStopRadialGradientBrushElement CenterPointString="50, 50" 
                                    RelativeBounds="4, 4, 93, 93" RelativeBoundsMeasure="Percent">
                                    <ColorStops>
                                        <igGaugeProp:ColorStop Color="Transparent" />
                                        <igGaugeProp:ColorStop Color="141, 141, 141" Stop="0.06206897" />
                                        <igGaugeProp:ColorStop Color="82, 82, 82" Stop="0.07586207" />
                                        <igGaugeProp:ColorStop Color="White" Stop="0.1448276" />
                                        <igGaugeProp:ColorStop Color="White" Stop="0.2448276" />
                                        <igGaugeProp:ColorStop Color="White" Stop="1" />
                                    </ColorStops>
                                </igGaugeProp:MultiStopRadialGradientBrushElement>
                            </BrushElements>
                        </igGaugeProp:BrushElementGroup>
                    </BrushElements>
                    <StrokeElement>
                        <BrushElements>
                            <igGaugeProp:SolidFillBrushElement Color="Silver" />
                        </BrushElements>
                    </StrokeElement>
                </Dial>
                <Scales>
                    <igGaugeProp:RadialGaugeScale EndAngle="403" StartAngle="135">
                        <MajorTickmarks EndExtent="79" EndWidth="3" Frequency="10" StartExtent="67" 
                            StartWidth="3">
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="189, 189, 189" />
                            </BrushElements>
                        </MajorTickmarks>
                        <MinorTickmarks EndExtent="78" EndWidth="1" StartExtent="73">
                            <StrokeElement>
                                <BrushElements>
                                    <igGaugeProp:SolidFillBrushElement Color="135, 135, 135" />
                                </BrushElements>
                            </StrokeElement>
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="240, 240, 240" />
                            </BrushElements>
                        </MinorTickmarks>
                        <Labels Extent="55" Font="Arial, 12px, style=Bold" Frequency="5" 
                            Orientation="Horizontal" SpanMaximum="18">
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="Black" />
                            </BrushElements>
                        </Labels>
                        <Markers>
                            <igGaugeProp:RadialGaugeNeedle EndExtent="65" EndWidth="1" MidExtent="0" 
                                MidWidth="3" StartExtent="-20" StartWidth="3" ValueString="25" 
                                WidthMeasure="Percent">
                                <Anchor RadiusMeasure="Percent">
                                    <BrushElements>
                                        <igGaugeProp:SimpleGradientBrushElement EndColor="64, 64, 64" 
                                            GradientStyle="BackwardDiagonal" StartColor="Gainsboro" />
                                    </BrushElements>
                                    <StrokeElement Thickness="2">
                                        <BrushElements>
                                            <igGaugeProp:RadialGradientBrushElement CenterColor="WhiteSmoke" 
                                                SurroundColor="Gray" />
                                        </BrushElements>
                                    </StrokeElement>
                                </Anchor>
                                <StrokeElement Thickness="0">
                                </StrokeElement>
                                <BrushElements>
                                    <igGaugeProp:SolidFillBrushElement Color="90, 145, 255" />
                                </BrushElements>
                            </igGaugeProp:RadialGaugeNeedle>
                        </Markers>
                        <Axes>
                            <igGaugeProp:NumericAxis EndValue="30" />
                        </Axes>
                    </igGaugeProp:RadialGaugeScale>
                </Scales>
            </igGaugeProp:RadialGauge>
        </Gauges>
        <Annotations>
            <igGaugeProp:BoxAnnotation Bounds="77, 135, 0, 0">
                <Label Font="Arial, 10pt, style=Bold" FormatString="Months">
                    <BrushElements>
                        <igGaugeProp:SolidFillBrushElement Color="White" />
                    </BrushElements>
                </Label>
                <BrushElements>
                    <igGaugeProp:SolidFillBrushElement Color="Silver" />
                </BrushElements>
            </igGaugeProp:BoxAnnotation>
        </Annotations>
        <DeploymentScenario FilePath="" ImageURL="#CLIENT_#SESSION.#EXT" />
    </igGauge:UltraGauge>
    <igGauge:UltraGauge ID="gTests" runat="server" BackColor="Transparent" 
        ForeColor="ControlLightLight" Height="181px" 
        style="z-index: 1; left: 423px; top: 58px; position: absolute; height: 181px; width: 216px" 
        Width="216px">
        <Gauges>
            <igGaugeProp:RadialGauge MarginString="2, 2, 2, 2, Pixels">
                <Dial>
                    <BrushElements>
                        <igGaugeProp:BrushElementGroup>
                            <BrushElements>
                                <igGaugeProp:MultiStopRadialGradientBrushElement CenterPointString="50, 50" 
                                    FocusScalesString="0.8, 0.8">
                                    <ColorStops>
                                        <igGaugeProp:ColorStop Color="161, 161, 161" />
                                        <igGaugeProp:ColorStop Color="96, 96, 96" Stop="0.4758621" />
                                        <igGaugeProp:ColorStop Color="89, 89, 89" Stop="1" />
                                    </ColorStops>
                                </igGaugeProp:MultiStopRadialGradientBrushElement>
                                <igGaugeProp:SolidFillBrushElement Color="Gray" RelativeBounds="4, 4, 93, 93" 
                                    RelativeBoundsMeasure="Percent" />
                                <igGaugeProp:MultiStopRadialGradientBrushElement CenterPointString="50, 50" 
                                    RelativeBounds="4, 4, 93, 93" RelativeBoundsMeasure="Percent">
                                    <ColorStops>
                                        <igGaugeProp:ColorStop Color="Transparent" />
                                        <igGaugeProp:ColorStop Color="141, 141, 141" Stop="0.06206897" />
                                        <igGaugeProp:ColorStop Color="82, 82, 82" Stop="0.07586207" />
                                        <igGaugeProp:ColorStop Color="White" Stop="0.1448276" />
                                        <igGaugeProp:ColorStop Color="White" Stop="0.2448276" />
                                        <igGaugeProp:ColorStop Color="White" Stop="1" />
                                    </ColorStops>
                                </igGaugeProp:MultiStopRadialGradientBrushElement>
                            </BrushElements>
                        </igGaugeProp:BrushElementGroup>
                    </BrushElements>
                    <StrokeElement>
                        <BrushElements>
                            <igGaugeProp:SolidFillBrushElement Color="Silver" />
                        </BrushElements>
                    </StrokeElement>
                </Dial>
                <Scales>
                    <igGaugeProp:RadialGaugeScale EndAngle="396" StartAngle="144">
                        <MajorTickmarks EndExtent="79" EndWidth="3" Frequency="10" StartExtent="67" 
                            StartWidth="3">
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="189, 189, 189" />
                            </BrushElements>
                        </MajorTickmarks>
                        <MinorTickmarks EndExtent="78" EndWidth="1" Frequency="2" StartExtent="73">
                            <StrokeElement>
                                <BrushElements>
                                    <igGaugeProp:SolidFillBrushElement Color="135, 135, 135" />
                                </BrushElements>
                            </StrokeElement>
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="240, 240, 240" />
                            </BrushElements>
                        </MinorTickmarks>
                        <Labels Extent="55" Font="Arial, 12px, style=Bold" Frequency="20" 
                            Orientation="Horizontal" SpanMaximum="18">
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="Black" />
                            </BrushElements>
                        </Labels>
                        <Markers>
                            <igGaugeProp:RadialGaugeNeedle EndExtent="65" EndWidth="1" MidExtent="0" 
                                MidWidth="3" StartExtent="-20" StartWidth="3" ValueString="25" 
                                WidthMeasure="Percent">
                                <Anchor RadiusMeasure="Percent">
                                    <BrushElements>
                                        <igGaugeProp:SimpleGradientBrushElement EndColor="64, 64, 64" 
                                            GradientStyle="BackwardDiagonal" StartColor="Gainsboro" />
                                    </BrushElements>
                                    <StrokeElement Thickness="2">
                                        <BrushElements>
                                            <igGaugeProp:RadialGradientBrushElement CenterColor="WhiteSmoke" 
                                                SurroundColor="Gray" />
                                        </BrushElements>
                                    </StrokeElement>
                                </Anchor>
                                <StrokeElement Thickness="0">
                                </StrokeElement>
                                <BrushElements>
                                    <igGaugeProp:SolidFillBrushElement Color="69, 209, 0" />
                                </BrushElements>
                            </igGaugeProp:RadialGaugeNeedle>
                            <igGaugeProp:RadialGaugeNeedle EndWidth="9" MidExtent="80" MidWidth="0" 
                                StartExtent="78" StartWidth="0" ValueString="70">
                                <BrushElements>
                                    <igGaugeProp:SolidFillBrushElement Color="38, 232, 0" />
                                </BrushElements>
                            </igGaugeProp:RadialGaugeNeedle>
                        </Markers>
                        <Axes>
                            <igGaugeProp:NumericAxis EndValue="100" />
                        </Axes>
                    </igGaugeProp:RadialGaugeScale>
                </Scales>
            </igGaugeProp:RadialGauge>
        </Gauges>
        <Annotations>
            <igGaugeProp:BoxAnnotation Bounds="69, 130, 82, 22">
                <Label Font="Arial, 10pt, style=Bold" FormatString="Avg Test %">
                    <BrushElements>
                        <igGaugeProp:SolidFillBrushElement Color="White" />
                    </BrushElements>
                </Label>
                <BrushElements>
                    <igGaugeProp:SolidFillBrushElement Color="Silver" />
                </BrushElements>
            </igGaugeProp:BoxAnnotation>
        </Annotations>
        <DeploymentScenario FilePath="" ImageURL="#CLIENT_#SESSION.#EXT" />
    </igGauge:UltraGauge>
    <igGauge:UltraGauge ID="gReporting" runat="server" BackColor="Transparent" 
        ForeColor="ControlLightLight" Height="181px" 
        style="z-index: 1; left: 606px; top: 77px; position: absolute; height: 181px; width: 216px" 
        Width="216px">
        <Gauges>
            <igGaugeProp:RadialGauge MarginString="2, 2, 2, 2, Pixels">
                <Dial>
                    <BrushElements>
                        <igGaugeProp:BrushElementGroup>
                            <BrushElements>
                                <igGaugeProp:MultiStopRadialGradientBrushElement CenterPointString="50, 50" 
                                    FocusScalesString="0.8, 0.8">
                                    <ColorStops>
                                        <igGaugeProp:ColorStop Color="161, 161, 161" />
                                        <igGaugeProp:ColorStop Color="96, 96, 96" Stop="0.4758621" />
                                        <igGaugeProp:ColorStop Color="89, 89, 89" Stop="1" />
                                    </ColorStops>
                                </igGaugeProp:MultiStopRadialGradientBrushElement>
                                <igGaugeProp:SolidFillBrushElement Color="Gray" RelativeBounds="4, 4, 93, 93" 
                                    RelativeBoundsMeasure="Percent" />
                                <igGaugeProp:MultiStopRadialGradientBrushElement CenterPointString="50, 50" 
                                    RelativeBounds="4, 4, 93, 93" RelativeBoundsMeasure="Percent">
                                    <ColorStops>
                                        <igGaugeProp:ColorStop Color="Transparent" />
                                        <igGaugeProp:ColorStop Color="141, 141, 141" Stop="0.06206897" />
                                        <igGaugeProp:ColorStop Color="82, 82, 82" Stop="0.07586207" />
                                        <igGaugeProp:ColorStop Color="White" Stop="0.1448276" />
                                        <igGaugeProp:ColorStop Color="White" Stop="0.2448276" />
                                        <igGaugeProp:ColorStop Color="White" Stop="1" />
                                    </ColorStops>
                                </igGaugeProp:MultiStopRadialGradientBrushElement>
                            </BrushElements>
                        </igGaugeProp:BrushElementGroup>
                    </BrushElements>
                    <StrokeElement>
                        <BrushElements>
                            <igGaugeProp:SolidFillBrushElement Color="Silver" />
                        </BrushElements>
                    </StrokeElement>
                </Dial>
                <Scales>
                    <igGaugeProp:RadialGaugeScale EndAngle="396" StartAngle="144">
                        <MajorTickmarks EndExtent="79" EndWidth="3" Frequency="10" StartExtent="67" 
                            StartWidth="3">
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="189, 189, 189" />
                            </BrushElements>
                        </MajorTickmarks>
                        <MinorTickmarks EndExtent="78" EndWidth="1" Frequency="2" StartExtent="73">
                            <StrokeElement>
                                <BrushElements>
                                    <igGaugeProp:SolidFillBrushElement Color="135, 135, 135" />
                                </BrushElements>
                            </StrokeElement>
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="240, 240, 240" />
                            </BrushElements>
                        </MinorTickmarks>
                        <Labels Extent="55" Font="Arial, 12px, style=Bold" Frequency="20" 
                            Orientation="Horizontal" SpanMaximum="18">
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="Black" />
                            </BrushElements>
                        </Labels>
                        <Markers>
                            <igGaugeProp:RadialGaugeNeedle EndExtent="65" EndWidth="1" MidExtent="0" 
                                MidWidth="3" StartExtent="-20" StartWidth="3" ValueString="25" 
                                WidthMeasure="Percent">
                                <Anchor RadiusMeasure="Percent">
                                    <BrushElements>
                                        <igGaugeProp:SimpleGradientBrushElement EndColor="64, 64, 64" 
                                            GradientStyle="BackwardDiagonal" StartColor="Gainsboro" />
                                    </BrushElements>
                                    <StrokeElement Thickness="2">
                                        <BrushElements>
                                            <igGaugeProp:RadialGradientBrushElement CenterColor="WhiteSmoke" 
                                                SurroundColor="Gray" />
                                        </BrushElements>
                                    </StrokeElement>
                                </Anchor>
                                <StrokeElement Thickness="0">
                                </StrokeElement>
                                <BrushElements>
                                    <igGaugeProp:SolidFillBrushElement Color="255, 61, 22" />
                                </BrushElements>
                            </igGaugeProp:RadialGaugeNeedle>
                            <igGaugeProp:RadialGaugeNeedle EndWidth="9" MidExtent="80" MidWidth="0" 
                                PrecisionString="0" StartExtent="78" StartWidth="0" ValueString="70">
                                <BrushElements>
                                    <igGaugeProp:SolidFillBrushElement Color="38, 232, 0" />
                                </BrushElements>
                            </igGaugeProp:RadialGaugeNeedle>
                        </Markers>
                        <Axes>
                            <igGaugeProp:NumericAxis EndValue="100" />
                        </Axes>
                    </igGaugeProp:RadialGaugeScale>
                </Scales>
            </igGaugeProp:RadialGauge>
        </Gauges>
        <Annotations>
            <igGaugeProp:BoxAnnotation Bounds="69, 130, 82, 22">
                <Label Font="Arial, 10pt, style=Bold" FormatString="Reporting %">
                    <BrushElements>
                        <igGaugeProp:SolidFillBrushElement Color="White" />
                    </BrushElements>
                </Label>
                <BrushElements>
                    <igGaugeProp:SolidFillBrushElement Color="Silver" />
                </BrushElements>
            </igGaugeProp:BoxAnnotation>
        </Annotations>
        <DeploymentScenario FilePath="" ImageURL="#CLIENT_#SESSION.#EXT" 
            Mode="Session" />
    </igGauge:UltraGauge>

    <input id="hShopID" type="hidden" runat=server />

</form>
</body>
</html>
