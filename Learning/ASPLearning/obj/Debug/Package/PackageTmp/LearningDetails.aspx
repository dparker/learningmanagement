﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LearningDetails.aspx.vb" Inherits="ASPLearning.LearningDetails" %>

<%@ Register assembly="Infragistics4.WebUI.UltraWebChart.v11.1, Version=11.1.20111.1006, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.WebUI.UltraWebChart" tagprefix="igchart" %>
<%@ Register assembly="Infragistics4.WebUI.UltraWebChart.v11.1, Version=11.1.20111.1006, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.UltraChart.Resources.Appearance" tagprefix="igchartprop" %>
<%@ Register assembly="Infragistics4.WebUI.UltraWebChart.v11.1, Version=11.1.20111.1006, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.UltraChart.Data" tagprefix="igchartdata" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pProgram" runat="server" BackColor="#33CC33" Height="26px" 
        Width="100%" 
        style="z-index: 1; left: 9px; top: 52px; position: absolute; height: 26px; width: 100%; margin-top: 0px">
    </asp:Panel>
    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="20pt" 
        style="z-index: 1; left: 10px; top: 19px; position: absolute; height: 24px" 
        Text="Program Progress"></asp:Label>
    <asp:Label ID="lEnd" runat="server" 
        style="z-index: 1;  top: 84px; position: absolute; right:10px" 
        Text="lEnd"></asp:Label>
    <hr style="z-index: 1; left: 10px; top: 75px; position: absolute; height: 2px; width: 100%" />
    <asp:Label ID="lMonths" runat="server" 
        style="z-index: 1; left: 45%; top: 84px; position: absolute" Text="lStart"></asp:Label>
    <asp:Label ID="lStart" runat="server" 
        style="z-index: 1; left: 12px; top: 84px; position: absolute" Text="lStart"></asp:Label>
    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="20pt" 
        style="z-index: 1; left: 10px; top: 120px; position: absolute; height: 24px" 
        Text="Learning Program Progress"></asp:Label>

    <asp:Panel ID="pCourses" runat="server" BackColor="#0033CC" Height="26px" Width="100%"     
        style="z-index: 1; left: 9px; top: 152px; position: absolute; height: 26px; margin-top: 0px">
    </asp:Panel>
    <hr style="z-index: 1; left: 10px; top: 174px; position: absolute; height: 2px; width: 100%" />
    <asp:Label ID="lProgram0" runat="server" 
        style="z-index: 1; left: 12px; top: 183px; position: absolute" Text="0%"></asp:Label>
    <asp:Label ID="Program100" runat="server" 
        style="z-index: 1;  top: 183px; position: absolute; right:10px" 
        Text="100%"></asp:Label>
    <asp:Label ID="lProgramInfo" runat="server" 
        style="z-index: 1; left: 45%; top: 183px; position: absolute" Text="lStart"></asp:Label>

    <table id="tProgram" runat="server" 
        style="width:100%;top: 195px; position: absolute; left: 10px; height: 38px;">
    </table>
    <asp:Panel ID="Panel1" runat="server">
    </asp:Panel>

    </form>
<div>
</div>
    </body>
</html>
