﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WebForm1.aspx.vb" Inherits="ASPLearning.WebForm1" %>

<%@ Register assembly="Infragistics4.WebUI.UltraWebGauge.v11.1, Version=11.1.20111.1006, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.WebUI.UltraWebGauge" tagprefix="igGauge" %>
<%@ Register assembly="Infragistics4.WebUI.UltraWebGauge.v11.1, Version=11.1.20111.1006, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.UltraGauge.Resources" tagprefix="igGaugeProp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    <igGauge:UltraGauge ID="UltraGauge1" runat="server" BackColor="Transparent" 
        ForeColor="ControlLightLight" Height="250px" Width="250px">
        <gauges>
            <igGaugeProp:RadialGauge MarginString="2, 2, 2, 2, Pixels">
                <dial>
                    <brushelements>
                        <igGaugeProp:BrushElementGroup>
                            <brushelements>
                                <igGaugeProp:MultiStopRadialGradientBrushElement CenterPointString="50, 50" 
                                    FocusScalesString="0.8, 0.8">
                                    <colorstops>
                                        <igGaugeProp:ColorStop Color="240, 240, 240" />
                                        <igGaugeProp:ColorStop Color="195, 195, 195" Stop="0.3413793" />
                                        <igGaugeProp:ColorStop Color="195, 195, 195" Stop="1" />
                                    </colorstops>
                                </igGaugeProp:MultiStopRadialGradientBrushElement>
                                <igGaugeProp:MultiStopRadialGradientBrushElement CenterPointString="50, 50" 
                                    RelativeBounds="4, 4, 93, 93" RelativeBoundsMeasure="Percent">
                                    <colorstops>
                                        <igGaugeProp:ColorStop Color="210, 210, 210" />
                                        <igGaugeProp:ColorStop Color="225, 225, 225" Stop="0.03989592" />
                                        <igGaugeProp:ColorStop Color="240, 240, 240" Stop="0.05030356" />
                                        <igGaugeProp:ColorStop Color="240, 240, 240" Stop="0.1006071" />
                                        <igGaugeProp:ColorStop Color="White" Stop="1" />
                                    </colorstops>
                                </igGaugeProp:MultiStopRadialGradientBrushElement>
                            </brushelements>
                        </igGaugeProp:BrushElementGroup>
                    </brushelements>
                    <strokeelement>
                        <brushelements>
                            <igGaugeProp:SolidFillBrushElement Color="Silver" />
                        </brushelements>
                    </strokeelement>
                </dial>
                <OverDial>
                    <BrushElements>
                        <igGaugeProp:BrushElementGroup>
                            <BrushElements>
                                <igGaugeProp:MultiStopRadialGradientBrushElement CenterPointString="8, 100" 
                                    FocusScalesString="5, 0">
                                    <ColorStops>
                                        <igGaugeProp:ColorStop Color="50, 255, 255, 255" />
                                        <igGaugeProp:ColorStop Color="150, 255, 255, 255" Stop="0.3310345" />
                                        <igGaugeProp:ColorStop Color="Transparent" Stop="0.3359606" />
                                        <igGaugeProp:ColorStop Color="Transparent" Stop="1" />
                                    </ColorStops>
                                </igGaugeProp:MultiStopRadialGradientBrushElement>
                            </BrushElements>
                        </igGaugeProp:BrushElementGroup>
                    </BrushElements>
                </OverDial>
                <scales>
                    <igGaugeProp:RadialGaugeScale EndAngle="405" StartAngle="135">
                        <majortickmarks endextent="79" endwidth="3" frequency="10" startextent="67" 
                            startwidth="3">
                            <brushelements>
                                <igGaugeProp:SolidFillBrushElement Color="Gray" />
                            </brushelements>
                        </majortickmarks>
                        <minortickmarks endextent="78" endwidth="1" frequency="2" startextent="73">
                            <strokeelement>
                                <brushelements>
                                    <igGaugeProp:SolidFillBrushElement Color="135, 135, 135" />
                                </brushelements>
                            </strokeelement>
                            <brushelements>
                                <igGaugeProp:SolidFillBrushElement Color="240, 240, 240" />
                            </brushelements>
                        </minortickmarks>
                        <labels extent="55" font="Arial, 8pt, style=Bold" frequency="20" 
                            orientation="Horizontal">
                            <brushelements>
                                <igGaugeProp:SolidFillBrushElement Color="64, 64, 64" />
                            </brushelements>
                        </labels>
                        <markers>
                            <igGaugeProp:RadialGaugeNeedle EndExtent="65" EndWidth="3" MidExtent="0" 
                                MidWidth="5" StartExtent="-20" StartWidth="5" ValueString="95">
                                <anchor radius="9" radiusmeasure="Percent">
                                    <brushelements>
                                        <igGaugeProp:SimpleGradientBrushElement EndColor="64, 64, 64" 
                                            GradientStyle="BackwardDiagonal" StartColor="Gainsboro" />
                                    </brushelements>
                                    <strokeelement thickness="2">
                                        <brushelements>
                                            <igGaugeProp:RadialGradientBrushElement CenterColor="WhiteSmoke" 
                                                SurroundColor="Gray" />
                                        </brushelements>
                                    </strokeelement>
                                </anchor>
                                <strokeelement thickness="0">
                                </strokeelement>
                                <brushelements>
                                    <igGaugeProp:SolidFillBrushElement Color="255, 61, 22" />
                                </brushelements>
                            </igGaugeProp:RadialGaugeNeedle>
                        </markers>
                        <axes>
                            <igGaugeProp:NumericAxis EndValue="100" />
                        </axes>
                    </igGaugeProp:RadialGaugeScale>
                </scales>
            </igGaugeProp:RadialGauge>
        </gauges>
    </igGauge:UltraGauge>
    </form>
</body>
</html>
