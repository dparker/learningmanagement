﻿Public Class LearningDetails1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetData()
        End If
    End Sub
    Private Sub GetData()
        Dim ds As DataSet
        Dim ProgressMonths As Integer = 0
        Dim TotalMonths As Integer = 0
        Dim TotalProgress As Integer = 0
        Dim MaximumCredits As Integer = 0
        Dim RequiredCredits As Integer = 0
        Dim ObtainedCredits As Integer = 0
        Dim TotalTest As Integer = 0
        Dim Totalcount As Integer = 0
        Dim CustomerID As String = ""
        Dim ProgressWeeks As Integer = 0
        Dim WeeksReported As Integer = 0
        Dim StartDate As String = "01/01/1900"
        ds = GeneralDataSet("EXEC LearningTally @PortalShopID = " + Request.QueryString("PortalShopID"))


        If ds.Tables.Count > 1 Then
            If ds.Tables(0).Rows.Count > 0 Then
                ProgressMonths = Val(ds.Tables(0).Rows(0).Item("ProgressMonths").ToString)
                TotalMonths = Val(ds.Tables(0).Rows(0).Item("TotalMonths").ToString)
                MaximumCredits = Val(ds.Tables(0).Rows(0).Item("MaximumCredits").ToString)
                RequiredCredits = Val(ds.Tables(0).Rows(0).Item("CreditsRequired").ToString)
                CustomerID = ds.Tables(0).Rows(0).Item("CustomerID").ToString
                ProgressWeeks = ds.Tables(0).Rows(0).Item("ProgressWeeks").ToString
                lTitle.Text = ds.Tables(0).Rows(0).Item("Program").ToString + " - " + ds.Tables(0).Rows(0).Item("Name").ToString
                StartDate = ds.Tables(0).Rows(0).Item("StartDate").ToString
            End If
            For x = 0 To ds.Tables(1).Rows.Count - 1
                With ds.Tables(1).Rows(x)
                    If .Item("tally") > 0 Then ObtainedCredits = ObtainedCredits + .Item("credits")
                End With
            Next
        End If

        Dim pd As PortalData.PortalDataSoapClient = New PortalData.PortalDataSoapClient("PortalDataSoap")
        Dim rd As DataSet
        rd = pd.GetData("select COUNT(*) from shop_performances where shop_performances.sperf_shop_id=" + Request.QueryString("PortalShopID") + " and sperf_date>='" + StartDate + "'", "Bubba985")
        If Not rd Is Nothing Then
            If rd.Tables(0).Rows.Count > 0 Then
                WeeksReported = Val(rd.Tables(0).Rows(0).Item(0).ToString)
            End If
        End If
        If WeeksReported > ProgressWeeks Then WeeksReported = ProgressWeeks
        For x = 0 To ds.Tables(2).Rows.Count - 1
            With ds.Tables(2).Rows(x)
                If .Item("grade") >= 70 Then ObtainedCredits = ObtainedCredits + .Item("credits")
                If .Item("grade") > -1 Then
                    TotalTest = TotalTest + .Item("grade")
                    Totalcount = Totalcount + 1
                End If
            End With
        Next
        For x = 0 To ds.Tables(3).Rows.Count - 1
            With ds.Tables(3).Rows(x)
                ObtainedCredits = ObtainedCredits + .Item("points")
            End With
        Next

        Dim Axis
        Axis = CType(UltraGauge1.Gauges(0), Infragistics.UltraGauge.Resources.LinearGauge).Scales(0).Axes(0)
        CType(Axis, Infragistics.UltraGauge.Resources.TimeAxis).StartValue = CDate(ds.Tables(0).Rows(0).Item("StartDate").ToString)
        CType(Axis, Infragistics.UltraGauge.Resources.TimeAxis).EndValue = CDate(ds.Tables(0).Rows(0).Item("EndDate").ToString)
        CType(UltraGauge1.Gauges(0), Infragistics.UltraGauge.Resources.LinearGauge).Scales(0).Markers(0).Value = Today

        Axis = CType(UltraGauge2.Gauges(0), Infragistics.UltraGauge.Resources.LinearGauge).Scales(0).Axes(0)
        CType(Axis, Infragistics.UltraGauge.Resources.NumericAxis).StartValue = 0
        CType(Axis, Infragistics.UltraGauge.Resources.NumericAxis).EndValue = MaximumCredits
        CType(Axis, Infragistics.UltraGauge.Resources.NumericAxis).SetTickmarkInterval(MaximumCredits / 40)

        Axis = CType(UltraGauge3.Gauges(0), Infragistics.UltraGauge.Resources.LinearGauge).Scales(0).Axes(0)
        CType(Axis, Infragistics.UltraGauge.Resources.NumericAxis).StartValue = 0
        CType(Axis, Infragistics.UltraGauge.Resources.NumericAxis).EndValue = ProgressWeeks
        CType(Axis, Infragistics.UltraGauge.Resources.NumericAxis).SetTickmarkInterval(ProgressWeeks / 40)
        CType(UltraGauge3.Gauges(0), Infragistics.UltraGauge.Resources.LinearGauge).Scales(0).Markers(0).Value = WeeksReported
        CType(UltraGauge3.Gauges(0), Infragistics.UltraGauge.Resources.LinearGauge).Scales(0).Markers(1).Value = ProgressWeeks * 0.7
        CType(UltraGauge3.Annotations(0), Infragistics.UltraGauge.Resources.BoxAnnotation).Label.FormatString = "Financial Reporting: " + WeeksReported.ToString + " of " + ProgressWeeks.ToString + " weeks"

        CType(UltraGauge2.Gauges(0), Infragistics.UltraGauge.Resources.LinearGauge).Scales(0).Markers(1).Value = RequiredCredits
        CType(UltraGauge2.Gauges(0), Infragistics.UltraGauge.Resources.LinearGauge).Scales(0).Markers(0).Value = ObtainedCredits
        CType(UltraGauge2.Annotations(0), Infragistics.UltraGauge.Resources.BoxAnnotation).Label.FormatString = "Course Credits - Achieved: " + ObtainedCredits.ToString + " Required: " + RequiredCredits.ToString



        Dim c As TableCell
        Dim tr As TableRow
        Dim h As TableHeaderCell
        tr = New TableRow
        tr.Style.Add("font-size", "8 pt")
        tr.Style.Add("Background-Color", "#ADB9CD")
        h = New TableHeaderCell
        h.Text = ""
        h.Width = 12
        tr.Cells.Add(h)
        h = New TableHeaderCell
        h.Text = "Course Description"
        tr.Cells.Add(h)
        h = New TableHeaderCell
        h.Text = "Credits"
        tr.Cells.Add(h)
        h.Width = 50
        Table1.Rows.Add(tr)

        For x = 0 To ds.Tables(1).Rows.Count - 1
            tr = New TableRow
            tr.BorderWidth = 2
            tr.BorderStyle = BorderStyle.Solid
            tr.BorderColor = Drawing.Color.LightGray
            tr.Style.Add("font-size", "8 pt")
            tr.Style.Add("Background-Color", "#FFFF99")
            c = New TableCell
            c.BorderColor = Drawing.Color.DarkGray
            c.BorderStyle = BorderStyle.Solid
            c.BorderWidth = 1
            'c.Style.Add("-ms-Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
            'c.Style.Add("Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
            'c.Style.Add("box-shadow", "3px 3px 4px #000")
            'c.Style.Add("-moz-box-shadow", "3px 3px 4px #000")
            'c.Style.Add("-webkit-box-shadow", "3px 3px 4px #000")
            c.Text = "<img align='center' height='12' src='" + IIf(ds.Tables(1).Rows(x).Item("Tally") > 0, "images\Yes_check.png", "images\X_mark.png") + "'/>"
            tr.Cells.Add(c)
            c = New TableCell
            'c.Style.Add("-ms-Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
            'c.Style.Add("Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
            'c.Style.Add("box-shadow", "3px 3px 4px #000")
            'c.Style.Add("-moz-box-shadow", "3px 3px 4px #000")
            'c.Style.Add("-webkit-box-shadow", "3px 3px 4px #000")
            c.Text = ds.Tables(1).Rows(x).Item("Seminar").ToString
            tr.Cells.Add(c)
            c = New TableCell
            'c.Style.Add("-ms-Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
            'c.Style.Add("Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
            'c.Style.Add("box-shadow", "3px 3px 4px #000")
            'c.Style.Add("-moz-box-shadow", "3px 3px 4px #000")
            'c.Style.Add("-webkit-box-shadow", "3px 3px 4px #000")
            c.Text = ds.Tables(1).Rows(x).Item("Seminar").ToString
            tr.Cells.Add(c)
            c.Text = IIf(ds.Tables(1).Rows(x).Item("Tally") > 0, ds.Tables(1).Rows(x).Item("Credits").ToString, "0")
            Table1.Rows.Add(tr)
        Next x
        'For x = 0 To ds.Tables(1).Rows.Count - 1
        '    c = New TableCell
        '    c.Text = ds.Tables(1).Rows(x).Item("Seminar").ToString + "<br/><br/>Credits: " + IIf(ds.Tables(1).Rows(x).Item("Tally") > 0, ds.Tables(1).Rows(x).Item("Credits").ToString, "0") + "<br/><br/><img src='" + IIf(ds.Tables(1).Rows(x).Item("Tally") > 0, "\images\Yes_check.png", "images\X_mark.png") + "'/>"
        '    c.Style.Add("font-size", "12pt")
        '    c.Style.Add("Background-Color", "#FFFF99")
        '    c.Style.Add("Width", "16.6%")
        '    c.Style.Add("Height", "120px")
        '    c.Style.Add("Text-Align", "Center")
        '    c.Style.Add("vertical-align", "Top")
        '    c.Style.Add("-ms-Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
        '    c.Style.Add("Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
        '    c.Style.Add("box-shadow", "3px 3px 4px #000")
        '    c.Style.Add("-moz-box-shadow", "3px 3px 4px #000")
        '    c.Style.Add("-webkit-box-shadow", "3px 3px 4px #000")
        '    tr.Cells.Add(c)
        '    If Int((x + 1) / 6) = (x + 1) / 6 Then
        '        Table1.Rows.Add(tr)
        '        tr = New TableRow
        '    End If
        'Next
        'If tr.Cells.Count > 0 Then Table1.Rows.Add(tr)


        tr = New TableRow
        tr.Style.Add("font-size", "8pt")
        tr.Style.Add("Background-Color", "#ADB9CD")
        tr.BorderWidth = 2
        tr.BorderStyle = BorderStyle.Solid
        tr.BorderColor = Drawing.Color.Gray
        h = New TableHeaderCell
        h.Text = ""
        h.Width = 12
        h.Style.Add("font-size", "10pt")
        tr.Cells.Add(h)
        h = New TableHeaderCell
        h.Style.Add("font-size", "10pt")
        h.Text = "Test Name"
        tr.Cells.Add(h)
        h = New TableHeaderCell
        h.Text = "Grade"
        tr.Cells.Add(h)
        h.Width = 50
        h.Style.Add("font-size", "10pt")
        h = New TableHeaderCell
        h.Text = "Credits"
        tr.Cells.Add(h)
        h.Width = 50
        h.Style.Add("font-size", "10pt")
        Table2.Rows.Add(tr)

        For x = 0 To ds.Tables(2).Rows.Count - 1
            With ds.Tables(2).Rows(x)
                tr = New TableRow
                tr.Style.Add("font-size", "10pt")
                tr.Style.Add("Background-Color", "#FFFF99")
                tr.BorderWidth = 2
                tr.BorderStyle = BorderStyle.Solid
                tr.BorderColor = Drawing.Color.LightGray
                c = New TableCell
                c.BorderColor = Drawing.Color.DarkGray
                c.BorderStyle = BorderStyle.Solid
                c.BorderWidth = 1
                'c.Style.Add("-ms-Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
                'c.Style.Add("Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
                'c.Style.Add("box-shadow", "3px 3px 4px #000")
                'c.Style.Add("-moz-box-shadow", "3px 3px 4px #000")
                'c.Style.Add("-webkit-box-shadow", "3px 3px 4px #000")
                c.Text = "<img  height='12' src='" + IIf(.Item("Grade") > 69, "images\Yes_check.png", "images\X_mark.png") + "'/>"
                tr.Cells.Add(c)
                c = New TableCell
                'c.Style.Add("-ms-Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
                'c.Style.Add("Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
                'c.Style.Add("box-shadow", "3px 3px 4px #000")
                'c.Style.Add("-moz-box-shadow", "3px 3px 4px #000")
                'c.Style.Add("-webkit-box-shadow", "3px 3px 4px #000")
                c.Text = "<a href='Test.aspx?CustomerID=" + CustomerID + "&TestID=" + ds.Tables(2).Rows(x).Item("TestID").ToString + "'>" + .Item("testname").ToString + " (" + .Item("retakecount").ToString + ")</a>"
                tr.Cells.Add(c)
                c = New TableCell
                'c.Style.Add("-ms-Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
                'c.Style.Add("Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
                'c.Style.Add("box-shadow", "3px 3px 4px #000")
                'c.Style.Add("-moz-box-shadow", "3px 3px 4px #000")
                'c.Style.Add("-webkit-box-shadow", "3px 3px 4px #000")
                c.Text = .Item("Grade").ToString + "%"
                tr.Cells.Add(c)
                c = New TableCell
                'c.Style.Add("-ms-Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
                'c.Style.Add("Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
                'c.Style.Add("box-shadow", "3px 3px 4px #000")
                'c.Style.Add("-moz-box-shadow", "3px 3px 4px #000")
                'c.Style.Add("-webkit-box-shadow", "3px 3px 4px #000")
                c.Text = IIf(.Item("Grade") > 69, ds.Tables(2).Rows(x).Item("Credits").ToString, "0")
                tr.Cells.Add(c)
                Table2.Rows.Add(tr)
            End With
        Next x



        'tr = New TableRow
        'For x = 0 To ds.Tables(2).Rows.Count - 1
        '    With ds.Tables(2).Rows(x)
        '        c = New TableCell
        '        c.Text = .Item("Testname").ToString + "<br/><br/>Credits: " + IIf(.Item("Grade") > 69, ds.Tables(2).Rows(x).Item("Credits").ToString, "0") + "<br/>" + IIf(.Item("grade") > -1, "Grade: " + .Item("grade").ToString, "") + "<br/><a href='Test.aspx?CustomerID=426&TestID=" + ds.Tables(2).Rows(x).Item("TestID").ToString + "'>" + IIf(.Item("grade") > -1, "Retake Test (" + .Item("retakecount").ToString + ")", "Take Test") + "</a>"
        '    End With
        '    c.Style.Add("font-size", "12pt")
        '    c.Style.Add("Background-Color", "#FFFF99")
        '    c.Style.Add("Width", "16.6%")
        '    c.Style.Add("Height", "120px")
        '    c.Style.Add("Text-Align", "Center")
        '    c.Style.Add("vertical-align", "Top")
        '    c.Style.Add("-ms-Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
        '    c.Style.Add("Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
        '    c.Style.Add("box-shadow", "3px 3px 4px #000")
        '    c.Style.Add("-moz-box-shadow", "3px 3px 4px #000")
        '    c.Style.Add("-webkit-box-shadow", "3px 3px 4px #000")
        '    tr.Cells.Add(c)
        '    If Int((x + 1) / 6) = (x + 1) / 6 Then
        '        Table2.Rows.Add(tr)
        '        tr = New TableRow
        '    End If
        'Next
        'If tr.Cells.Count > 0 Then Table2.Rows.Add(tr)

        tr = New TableRow
        tr.Style.Add("font-size", "8pt")
        tr.Style.Add("Background-Color", "#ADB9CD")
        tr.BorderWidth = 2
        tr.BorderStyle = BorderStyle.Solid
        tr.BorderColor = Drawing.Color.Gray
        h = New TableHeaderCell
        h.Style.Add("font-size", "10pt")
        h.Text = "Extra Credit"
        tr.Cells.Add(h)
        h = New TableHeaderCell
        h.Text = "Credits"
        h.Style.Add("font-size", "10pt")
        tr.Cells.Add(h)
        h.Width = 50
        Table3.Rows.Add(tr)

        For x = 0 To ds.Tables(3).Rows.Count - 1
            With ds.Tables(3).Rows(x)
                tr = New TableRow
                tr.Style.Add("font-size", "8 pt")
                tr.Style.Add("Background-Color", "#FFFF99")
                tr.BorderWidth = 2
                tr.BorderStyle = BorderStyle.Solid
                tr.BorderColor = Drawing.Color.LightGray
                c = New TableCell
                'c.Style.Add("-ms-Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
                'c.Style.Add("Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
                'c.Style.Add("box-shadow", "3px 3px 4px #000")
                'c.Style.Add("-moz-box-shadow", "3px 3px 4px #000")
                'c.Style.Add("-webkit-box-shadow", "3px 3px 4px #000")
                c.Text = .Item("bonusname").ToString
                tr.Cells.Add(c)
                c = New TableCell
                'c.Style.Add("-ms-Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
                'c.Style.Add("Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
                'c.Style.Add("box-shadow", "3px 3px 4px #000")
                'c.Style.Add("-moz-box-shadow", "3px 3px 4px #000")
                'c.Style.Add("-webkit-box-shadow", "3px 3px 4px #000")
                c.Text = ds.Tables(1).Rows(x).Item("Seminar").ToString
                tr.Cells.Add(c)
                c.Text = .Item("Points").ToString
            End With
            Table3.Rows.Add(tr)
        Next x

        'tr = New TableRow
        'For x = 0 To ds.Tables(3).Rows.Count - 1
        '    With ds.Tables(3).Rows(x)
        '        c = New TableCell
        '        c.Text = .Item("bonusname").ToString + "<br/><br/>Credits: " + .Item("Points").ToString + "<br/>"
        '    End With
        '    c.Style.Add("font-size", "12pt")
        '    c.Style.Add("Background-Color", "#FFFF99")
        '    c.Style.Add("Width", "16.6%")
        '    c.Style.Add("Height", "120px")
        '    c.Style.Add("Text-Align", "Center")
        '    c.Style.Add("vertical-align", "Top")
        '    c.Style.Add("-ms-Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
        '    c.Style.Add("Filter", "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#000000')")
        '    c.Style.Add("box-shadow", "3px 3px 4px #000")
        '    c.Style.Add("-moz-box-shadow", "3px 3px 4px #000")
        '    c.Style.Add("-webkit-box-shadow", "3px 3px 4px #000")
        '    tr.Cells.Add(c)
        '    If Int((x + 1) / 6) = (x + 1) / 6 Then
        '        Table3.Rows.Add(tr)
        '        tr = New TableRow
        '    End If
        'Next
        'If tr.Cells.Count > 0 Then Table3.Rows.Add(tr)
    End Sub

End Class