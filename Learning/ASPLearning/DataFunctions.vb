﻿Module DataFunctions
    Public Function GetGUID() As String
        Dim MyGUID As New Guid
        MyGUID = Guid.NewGuid
        Return MyGUID.ToString
    End Function
    Public Function DataConnection() As System.Data.SqlClient.SqlConnection
        Try
            DataConnection = New System.Data.SqlClient.SqlConnection
            DataConnection.ConnectionString = ConfigurationSettings.AppSettings("ConnectionString.ATI")
            DataConnection.Open()
        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try

    End Function
    Public Sub RecycleConnection()
        Exit Sub
        If DataConnection.State = ConnectionState.Open Then
            DataConnection.Close()
            DataConnection.Open()
        End If
    End Sub
    Public Function DataReader(ByVal QueryString As String) As SqlClient.SqlDataReader
        Dim Connection As SqlClient.SqlConnection
        Connection = DataConnection()
        Try
            Dim x As String
            'RecycleConnection()
            If DataConnection.State = ConnectionState.Closed Then DataConnection.Open()
            Dim SelectCommand As New SqlClient.SqlCommand(QueryString, Connection)
            DataReader = SelectCommand.ExecuteReader()
            'Connection.Close()
            'Connection = Nothing
        Catch ex As Exception
            ex = ex
        End Try
    End Function
    Public Function GeneralDataSet(ByVal QueryString As String, Optional ByVal TableName As String = "") As DataSet
        Try
            RecycleConnection()
            GeneralDataSet = New DataSet
            'If DataConnection.State = ConnectionState.Closed Then DataConnection()
            Dim Connection As SqlClient.SqlConnection
            Connection = DataConnection()
            Dim SelectCommand As New SqlClient.SqlCommand(QueryString, Connection)
            Dim da As New SqlClient.SqlDataAdapter
            da.SelectCommand = SelectCommand
            If TableName = "" Then
                da.Fill(GeneralDataSet)
            Else
                da.Fill(GeneralDataSet, TableName)
            End If
            Connection.Close()
            Connection = Nothing

        Catch ex As Exception
            ex = ex
        End Try

    End Function
    Public Function GeneralQuery(ByVal QueryString As String)
        Try
            RecycleConnection()
            Dim Connection As SqlClient.SqlConnection
            Dim iRet As Integer
            Connection = DataConnection()
            'If DataConnection.State = ConnectionState.Closed Then DataConnection()
            Dim SelectCommand As New SqlClient.SqlCommand(QueryString, Connection)
            iRet = SelectCommand.ExecuteNonQuery()
            Connection.Close()
            Connection = Nothing

        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try

    End Function
    Public Function GeneralData(ByVal QueryString As String) As String
        Try
            RecycleConnection()
            GeneralData = ""
            Dim Connection As SqlClient.SqlConnection
            'Dim iRet As Integer
            Connection = DataConnection()
            Dim SelectCommand As New SqlClient.SqlCommand(QueryString, Connection)
            'SelectCommand = New SqlClient.SqlCommand(QueryString, Connection)
            'SelectCommand.CommandText = QueryString
            GeneralData = SelectCommand.ExecuteScalar.ToString
            Connection.Close()
            Connection = Nothing

        Catch ex As Exception
            GeneralData = ""
        End Try
    End Function

End Module
