﻿Imports System.Drawing
Imports System.Drawing.Imaging
Imports Infragistics.UltraGauge.Resources
Imports Infragistics.UltraGauge
Imports Infragistics.WebUI.UltraWebGauge
Public Class _Default

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then GetData()
    End Sub
    Private Sub GetData()
        Dim ds As DataSet
        Dim ProgressMonths As Integer = 0
        Dim TotalMonths As Integer = 0
        Dim TotalProgress As Integer = 0
        Dim MaximumCredits As Integer = 0
        Dim RequiredCredits As Integer = 0
        Dim ObtainedCredits As Integer = 0
        Dim TotalTest As Integer = 0
        Dim Totalcount As Integer = 0
        Dim ProgressWeeks As Integer = 0
        Dim WeeksReported As Integer = 0
        Dim ReportPercent As Integer
        Dim StartDate As String = ""
        hShopID.Value = Request.QueryString("PortalShopID").ToString
        ds = GeneralDataSet("EXEC LearningTally @PortalShopID = " + Request.QueryString("PortalShopID"))

        If ds.Tables.Count > 1 Then
            If ds.Tables(0).Rows.Count > 0 Then
                ProgressMonths = Val(ds.Tables(0).Rows(0).Item("ProgressMonths").ToString)
                TotalMonths = Val(ds.Tables(0).Rows(0).Item("TotalMonths").ToString)
                MaximumCredits = Val(ds.Tables(0).Rows(0).Item("MaximumCredits").ToString)
                RequiredCredits = Val(ds.Tables(0).Rows(0).Item("CreditsRequired").ToString)
                ProgressWeeks = Val(ds.Tables(0).Rows(0).Item("ProgressWeeks").ToString)
                StartDate = ds.Tables(0).Rows(0).Item("StartDate").ToString
            End If
            For x = 0 To ds.Tables(1).Rows.Count - 1
                With ds.Tables(1).Rows(x)
                    If .Item("tally") > 0 Then ObtainedCredits = ObtainedCredits + .Item("credits")
                End With
            Next
        Else
            gTests.Visible = False
            gLearning.Visible = False
            gMonths.Visible = False
            gReporting.Visible = False
            Label1.Text = ds.Tables(0).Rows(0).Item(0).ToString
            Exit Sub
        End If
        For x = 0 To ds.Tables(2).Rows.Count - 1
            With ds.Tables(2).Rows(x)
                If .Item("grade") >= 70 Then ObtainedCredits = ObtainedCredits + .Item("credits")
                If .Item("grade") > -1 Then
                    TotalTest = TotalTest + .Item("grade")
                    Totalcount = Totalcount + 1
                End If
            End With
        Next
        For x = 0 To ds.Tables(3).Rows.Count - 1
            With ds.Tables(3).Rows(x)
                ObtainedCredits = ObtainedCredits + .Item("points")
            End With
        Next
        If TotalMonths > 0 Then
            TotalProgress = Math.Round(100 * ProgressMonths / TotalMonths, 0)
        End If
        Dim pd As PortalData.PortalDataSoapClient = New PortalData.PortalDataSoapClient("PortalDataSoap")
        Dim rd As DataSet
        rd = pd.GetData("select COUNT(*) from shop_performances where shop_performances.sperf_shop_id=" + Request.QueryString("PortalShopID") + " and sperf_date>='" + StartDate + "'", "Bubba985")
        If Not rd Is Nothing Then
            If rd.Tables(0).Rows.Count > 0 Then
                WeeksReported = Val(rd.Tables(0).Rows(0).Item(0).ToString)
            End If
        End If
        If WeeksReported > ProgressWeeks Then WeeksReported = ProgressWeeks
        If ProgressWeeks > 0 Then
            ReportPercent = (WeeksReported / ProgressWeeks) * 100
        End If
        'Progress
        CType(gMonths.Gauges(0), Infragistics.UltraGauge.Resources.RadialGauge).Scales(0).Markers(0).Value = ProgressMonths
        'credits
        CType(gLearning.Gauges(0), Infragistics.UltraGauge.Resources.RadialGauge).Scales(0).Axis.SetEndValue(MaximumCredits / 10)
        CType(gLearning.Gauges(0), Infragistics.UltraGauge.Resources.RadialGauge).Scales(0).Axis.SetStartValue(0)
        CType(gLearning.Gauges(0), Infragistics.UltraGauge.Resources.RadialGauge).Scales(0).Axis.SetTickmarkInterval((MaximumCredits / 10) / 100)
        CType(gLearning.Gauges(0), Infragistics.UltraGauge.Resources.RadialGauge).Scales(0).Markers(1).Value = (RequiredCredits / 10)
        CType(gLearning.Gauges(0), Infragistics.UltraGauge.Resources.RadialGauge).Scales(0).Markers(0).Value = ObtainedCredits / 10
        'tests
        CType(gTests.Gauges(0), Infragistics.UltraGauge.Resources.RadialGauge).Scales(0).Markers(0).Value = IIf(Totalcount > 0, TotalTest / Totalcount, 0)
        If CType(gTests.Gauges(0), Infragistics.UltraGauge.Resources.RadialGauge).Scales(0).Markers(0).Value < 70 Then
            CType(CType(gTests.Gauges(0), Infragistics.UltraGauge.Resources.RadialGauge).Scales(0).Markers(0).BrushElement, SolidFillBrushElement).Color = System.Drawing.Color.FromArgb(255, 61, 22)
        End If
        'reporting
        CType(gReporting.Gauges(0), Infragistics.UltraGauge.Resources.RadialGauge).Scales(0).Markers(0).Value = ReportPercent
        If ReportPercent > 68 Then
            CType(CType(gReporting.Gauges(0), Infragistics.UltraGauge.Resources.RadialGauge).Scales(0).Markers(0).BrushElement, SolidFillBrushElement).Color = System.Drawing.Color.FromArgb(69, 209, 0)
        End If
    End Sub

End Class