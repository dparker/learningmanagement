﻿var MyView;
var MyStore;
var imageTpl;
Ext.application({
    name: 'Learning',
    launch: function () {
        Ext.define('Image', {
            extend: 'Ext.data.Model',
            fields: [
        { name: 'src', type: 'string' },
        { name: 'caption', type: 'string' }
        ]
        });
        imageTpl = new Ext.XTemplate(
            '<tpl for="."><div style="width:220px;padding:10px;border:5px solid gray;margin:0px;"><img src="{src}" /><br/><span>{caption}</span></div></tpl>'
            );
//        imageTpl = new Ext.XTemplate(
//            '<tpl for=".">',
//            '<div style="width:220px;padding:10px;border:5px solid gray;margin:0px;">',
//            '<img src="{src}" />',
//            '<br/><span>{caption}</span>',
//            '</div>',
//            '</tpl>'
//            );
//        imageTpl = new Ext.XTemplate(
//            '<tpl for=".">',
//            '<style type="text/css">div.ex{width:220px;padding:10px;border:5px solid gray;margin:0px;}</style>',
//            '<img src="{src}" />',
//            '<br/><span>{caption}</span>',
//            '</tpl>'
//            );
        var AddResult = new Ext.data.Connection();
        AddResult.request({
            url: "http://localhost/atidata/svc.asmx/XMLQuery?Query=EXEC%20LearningTally%20@LearningProgramID = 13,@CustomerID = 1886&TableName=Bubba985",
            //params: { a: 'EXEC LearningTally @LearningProgramID = 13,@CustomerID = 1886' , b: 'Bubba985' },
            method: 'Get',
            scope: this,
            callback: function (options, success, response) {
                if (success) {
                    MyStore = Ext.create('Ext.data.Store', {
                        id: 'imagesStore2',
                        model: 'Image'
                    });
                    var xml = response.responseXML;
                    var myresult = Ext.DomQuery.select('Table', xml, 'Not there');
                    for (i = 0; i < myresult.length; i++) {
                        var classTitle = myresult[i].childNodes[0].childNodes[0].data;
                        var Tally = myresult[i].childNodes[1].childNodes[0].data;
                        if (Tally == '0') { Tally = 'images/x_mark.png' } else { Tally = 'images/Yes_check.png' }
                        //                        //if (s[1].textContent != '') { s[1].textContent = 'Speaker: ' + s[1].textContent; }
                        MyStore.add({ src:  Tally, caption: classTitle });
                    }

                    Ext.create('Ext.view.View', {
                        store: Ext.data.StoreManager.lookup('imagesStore2'),
                        tpl: imageTpl,
                        emptyText: 'No images available',
                        renderTo: Ext.getBody()
                    });


                }
            }
        });



        //        Ext.create('Ext.container.Viewport', {
        //            layout: {
        //                type: 'fit',
        //                align: 'center'
        //            },
        //            renderTo: Ext.getBody(),
        //            items: [
        //                {
        //                    title: 'Learning Program',
        //                    html: 'Hello! Welcome to <a target="_blank" href="http://docs.sencha.com/ext-js/4-0/#/api/Ext" rel="Ext" class="docClass">Ext</a> JS.'
        //                }
        //            ]
        //        });

    }
});