﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LearningDetails1.aspx.vb" Inherits="ASPLearning.LearningDetails1" %>

<%@ Register assembly="Infragistics4.WebUI.UltraWebGauge.v11.1, Version=11.1.20111.1006, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.WebUI.UltraWebGauge" tagprefix="igGauge" %>
<%@ Register assembly="Infragistics4.WebUI.UltraWebGauge.v11.1, Version=11.1.20111.1006, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.UltraGauge.Resources" tagprefix="igGaugeProp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ATI Learning Manangement System</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="lTitle" runat="server" Text="Re-Enginering Program" 
            Font-Bold="True" Font-Size="X-Large"></asp:Label>
        <br />
        Progress Details<br />
        <br />
    </div>
    <igGauge:UltraGauge ID="UltraGauge1" runat="server" BackColor="Transparent" 
        Height="113px" Width="800px">
        <Gauges>
            <igGaugeProp:LinearGauge Bounds="0, 0, 800, 102" CornerExtent="17" 
                MarginString="2, 0, 2, 0, Pixels">
                <Scales>
                    <igGaugeProp:LinearGaugeScale EndExtent="98" StartExtent="2">
                        <MajorTickmarks EndExtent="53" EndWidth="2" Frequency="6" StartExtent="36" 
                            StartWidth="2">
                            <StrokeElement Color="Black" Thickness="2">
                            </StrokeElement>
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="Black" />
                            </BrushElements>
                        </MajorTickmarks>
                        <MinorTickmarks EndExtent="55" EndWidth="4" StartExtent="44" StartWidth="4">
                            <StrokeElement Thickness="0">
                            </StrokeElement>
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="141, 141, 0" />
                            </BrushElements>
                        </MinorTickmarks>
                        <Labels Extent="18" Font="Arial, 10pt, style=Bold" 
                            FormatString="&lt;DATA_VALUE:M/yyy&gt;" Frequency="6" RotationAngle="43">
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="MidnightBlue" />
                            </BrushElements>
                        </Labels>
                        <Markers>
                            <igGaugeProp:LinearGaugeBarMarker InnerExtent="53" OuterExtent="72" 
                                PrecisionString="" ValueString="03/06/2012 00:00:00">
                                <BrushElements>
                                    <igGaugeProp:SolidFillBrushElement Color="Brown" />
                                </BrushElements>
                            </igGaugeProp:LinearGaugeBarMarker>
                        </Markers>
                        <Axes>
                            <igGaugeProp:TimeAxis EndValue="2012-03-30" StartValue="2009-09-30" 
                                TickmarkInterval="30.00:00:00" />
                        </Axes>
                    </igGaugeProp:LinearGaugeScale>
                </Scales>
                <BrushElements>
                    <igGaugeProp:MultiStopLinearGradientBrushElement Angle="90">
                        <ColorStops>
                            <igGaugeProp:ColorStop Color="240, 240, 240" />
                            <igGaugeProp:ColorStop Color="240, 240, 240" Stop="0.6791444" />
                            <igGaugeProp:ColorStop Color="White" Stop="1" />
                        </ColorStops>
                    </igGaugeProp:MultiStopLinearGradientBrushElement>
                </BrushElements>
                <StrokeElement>
                    <BrushElements>
                        <igGaugeProp:SolidFillBrushElement Color="Silver" />
                    </BrushElements>
                </StrokeElement>
            </igGaugeProp:LinearGauge>
        </Gauges>
        <Annotations>
            <igGaugeProp:BoxAnnotation Bounds="24, 5, 0, 0">
                <Label Font="Arial, 9pt, style=Bold" FormatString="Program Duration">
                    <BrushElements>
                        <igGaugeProp:SolidFillBrushElement Color="RoyalBlue" />
                    </BrushElements>
                </Label>
                <BrushElements>
                    <igGaugeProp:SolidFillBrushElement Color="Transparent" />
                </BrushElements>
                <StrokeElement Thickness="0">
                </StrokeElement>
            </igGaugeProp:BoxAnnotation>
        </Annotations>
        <DeploymentScenario Mode="Session" />
    </igGauge:UltraGauge>
    <br />
    <br />
    <igGauge:UltraGauge ID="UltraGauge2" runat="server" BackColor="Transparent" 
        Height="111px" Width="800px">
        <Gauges>
            <igGaugeProp:LinearGauge Bounds="0, 0, 800, 102" CornerExtent="17" 
                MarginString="2, 0, 2, 0, Pixels">
                <Scales>
                    <igGaugeProp:LinearGaugeScale EndExtent="98" StartExtent="2">
                        <MajorTickmarks EndExtent="53" EndWidth="2" Frequency="2" StartExtent="36" 
                            StartWidth="2">
                            <StrokeElement Color="Black" Thickness="2">
                            </StrokeElement>
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="Black" />
                            </BrushElements>
                        </MajorTickmarks>
                        <MinorTickmarks EndExtent="55" EndWidth="4" StartExtent="44" StartWidth="4">
                            <StrokeElement Thickness="0">
                            </StrokeElement>
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="141, 141, 0" />
                            </BrushElements>
                        </MinorTickmarks>
                        <Labels Extent="18" Font="Arial, 10pt, style=Bold" Frequency="2">
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="MidnightBlue" />
                            </BrushElements>
                        </Labels>
                        <Markers>
                            <igGaugeProp:LinearGaugeBarMarker InnerExtent="53" OuterExtent="72" 
                                PrecisionString="0" ValueString="400">
                                <BrushElements>
                                    <igGaugeProp:SolidFillBrushElement Color="Brown" />
                                </BrushElements>
                            </igGaugeProp:LinearGaugeBarMarker>
                            <igGaugeProp:LinearGaugeNeedle EndWidth="20" MidWidth="8" PrecisionString="0" 
                                StartExtent="74" StartWidth="0" ValueString="425">
                                <BrushElements>
                                    <igGaugeProp:SolidFillBrushElement Color="19, 118, 0" />
                                </BrushElements>
                            </igGaugeProp:LinearGaugeNeedle>
                        </Markers>
                        <Axes>
                            <igGaugeProp:NumericAxis EndValue="850" TickmarkInterval="50" />
                        </Axes>
                    </igGaugeProp:LinearGaugeScale>
                </Scales>
                <BrushElements>
                    <igGaugeProp:MultiStopLinearGradientBrushElement Angle="90">
                        <ColorStops>
                            <igGaugeProp:ColorStop Color="240, 240, 240" />
                            <igGaugeProp:ColorStop Color="240, 240, 240" Stop="0.6791444" />
                            <igGaugeProp:ColorStop Color="White" Stop="1" />
                        </ColorStops>
                    </igGaugeProp:MultiStopLinearGradientBrushElement>
                </BrushElements>
                <StrokeElement>
                    <BrushElements>
                        <igGaugeProp:SolidFillBrushElement Color="Silver" />
                    </BrushElements>
                </StrokeElement>
            </igGaugeProp:LinearGauge>
        </Gauges>
        <Annotations>
            <igGaugeProp:BoxAnnotation Bounds="24, 5, 0, 0">
                <Label Font="Arial, 9pt, style=Bold" FormatString="Course Credits">
                    <BrushElements>
                        <igGaugeProp:SolidFillBrushElement Color="RoyalBlue" />
                    </BrushElements>
                </Label>
                <BrushElements>
                    <igGaugeProp:SolidFillBrushElement Color="Transparent" />
                </BrushElements>
                <StrokeElement Thickness="0">
                </StrokeElement>
            </igGaugeProp:BoxAnnotation>
        </Annotations>
        <DeploymentScenario Mode="Session" />
    </igGauge:UltraGauge>
    <br />
    <br />
    <igGauge:UltraGauge ID="UltraGauge3" runat="server" BackColor="Transparent" 
        Height="111px" Width="800px">
        <Gauges>
            <igGaugeProp:LinearGauge Bounds="0, 0, 800, 102" CornerExtent="17" 
                MarginString="2, 0, 2, 0, Pixels">
                <Scales>
                    <igGaugeProp:LinearGaugeScale EndExtent="98" StartExtent="2">
                        <MajorTickmarks EndExtent="53" EndWidth="2" Frequency="2" StartExtent="36" 
                            StartWidth="2">
                            <StrokeElement Color="Black" Thickness="2">
                            </StrokeElement>
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="Black" />
                            </BrushElements>
                        </MajorTickmarks>
                        <MinorTickmarks EndExtent="55" EndWidth="4" StartExtent="44" StartWidth="4">
                            <StrokeElement Thickness="0">
                            </StrokeElement>
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="141, 141, 0" />
                            </BrushElements>
                        </MinorTickmarks>
                        <Labels Extent="18" Font="Arial, 10pt, style=Bold" Frequency="2">
                            <BrushElements>
                                <igGaugeProp:SolidFillBrushElement Color="MidnightBlue" />
                            </BrushElements>
                        </Labels>
                        <Markers>
                            <igGaugeProp:LinearGaugeBarMarker InnerExtent="53" OuterExtent="72" 
                                PrecisionString="0" ValueString="400">
                                <BrushElements>
                                    <igGaugeProp:SolidFillBrushElement Color="Brown" />
                                </BrushElements>
                            </igGaugeProp:LinearGaugeBarMarker>
                            <igGaugeProp:LinearGaugeNeedle EndWidth="20" MidWidth="8" PrecisionString="0" 
                                StartExtent="74" StartWidth="0" ValueString="425">
                                <BrushElements>
                                    <igGaugeProp:SolidFillBrushElement Color="19, 118, 0" />
                                </BrushElements>
                            </igGaugeProp:LinearGaugeNeedle>
                        </Markers>
                        <Axes>
                            <igGaugeProp:NumericAxis EndValue="100" TickmarkInterval="5" />
                        </Axes>
                    </igGaugeProp:LinearGaugeScale>
                </Scales>
                <BrushElements>
                    <igGaugeProp:MultiStopLinearGradientBrushElement Angle="90">
                        <ColorStops>
                            <igGaugeProp:ColorStop Color="240, 240, 240" />
                            <igGaugeProp:ColorStop Color="240, 240, 240" Stop="0.6791444" />
                            <igGaugeProp:ColorStop Color="White" Stop="1" />
                        </ColorStops>
                    </igGaugeProp:MultiStopLinearGradientBrushElement>
                </BrushElements>
                <StrokeElement>
                    <BrushElements>
                        <igGaugeProp:SolidFillBrushElement Color="Silver" />
                    </BrushElements>
                </StrokeElement>
            </igGaugeProp:LinearGauge>
        </Gauges>
        <Annotations>
            <igGaugeProp:BoxAnnotation Bounds="24, 5, 0, 0">
                <Label Font="Arial, 9pt, style=Bold" FormatString="Financial Reporting">
                    <BrushElements>
                        <igGaugeProp:SolidFillBrushElement Color="RoyalBlue" />
                    </BrushElements>
                </Label>
                <BrushElements>
                    <igGaugeProp:SolidFillBrushElement Color="Transparent" />
                </BrushElements>
                <StrokeElement Thickness="0">
                </StrokeElement>
            </igGaugeProp:BoxAnnotation>
        </Annotations>
        <DeploymentScenario Mode="Session" />
    </igGauge:UltraGauge>
    <br />
    <asp:Label ID="Label1" runat="server" Text="Class Attendence" ForeColor="RoyalBlue" Font-Size="9" Font-Names="Arial" Font-Bold="true"></asp:Label>
    <asp:Table ID="Table1" runat="server" Width="800px" BorderWidth="2px" 
        CellSpacing="0">
    </asp:Table>
    <br />
    <asp:Label ID="Label2" runat="server" Text="Class Tests" ForeColor="RoyalBlue" Font-Size="9" Font-Names="Arial" Font-Bold="true"></asp:Label>
    <asp:Table ID="Table2" runat="server" Width="800px" BorderWidth="2px" 
        CellSpacing="0">
    </asp:Table>
    <br />
    <asp:Label ID="Label3" runat="server" Text="Extra Credits" ForeColor="RoyalBlue" Font-Size="9" Font-Names="Arial" Font-Bold="true"></asp:Label>
    <asp:Table ID="Table3" runat="server" Width="800px" BorderWidth="2px" 
        CellSpacing="0">
    </asp:Table>
    </form>
</body>
</html>
