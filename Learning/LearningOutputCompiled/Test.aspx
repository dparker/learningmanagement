﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Test.aspx.vb" Inherits="ASPLearning.Test" %>

<%@ Register Assembly="Infragistics4.Web.v11.1, Version=11.1.20111.1006, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI.LayoutControls" tagprefix="ig" %>
<%@ Register Assembly="Infragistics4.Web.v11.1, Version=11.1.20111.1006, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.Web.UI" tagprefix="ig" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>

    <script type="text/javascript">
        function CheckAnswer(r, c, l, a, q) {
            var tab = $find('<%=WebTab1.ClientID%>');
            var selTab = tab.get_tabs()[tab.get_selectedIndex()];
            var Question;
            var hXML = document.getElementById("hTestXML");
            var QuestionCount = Number(document.getElementById("hQuestionCount").value);
            var CorrectCount = Number(document.getElementById("hCorrectCount").value);
            var Grade = document.getElementById("lGrade");
            hXML.value = hXML.value + '{Item}{QuestionID}' + q + '{/QuestionID}{AnswerID}' + a + '{/AnswerID}{Correct}' + c + '{/Correct}{/Item}';
            if (navigator.appName == 'Microsoft Internet Explorer') {
                for (i = 0; i <= selTab._element.all.length - 1; i++) {
                    if (selTab._element.all[i].childNodes.length > 0) {
                        Question = selTab._element.all[i].firstChild.id;
                        if (Question != undefined) {
                            if (Question.indexOf("Answer") > 0) {
                                var q = document.getElementById(Question);
                                q.disabled = true;
                            }
                        }
                    }
                }
            } else {
                for (i = 0; i <= selTab._element.childNodes[1].childNodes[1].childNodes.length - 1; i++) {
                try {
                    Question = selTab._element.childNodes[1].childNodes[1].childNodes[i].childNodes[1].childNodes[0].childNodes[0].id;
                    }
                catch(err)
                    {
                    Question = undefined;
                    }
                    if (Question != undefined) {
                        if (Question.indexOf("Answer") > 0) {
                            var q = document.getElementById(Question);
                            q.disabled = true;
                        }
                    }
                }
            }
            var b = document.getElementById(l);
            b.style.visibility = "visible";
            if (c == '1') {
                b.style.color = "green";
                CorrectCount++;
            } else {
                b.style.color = "red";
                b.lastChild.nodeValue = "Wrong";
            }
            document.getElementById("hCorrectCount").value = CorrectCount;
            var per = Math.round((CorrectCount / QuestionCount) * 100);
            if (per > 69) { Grade.style.color = "green"; } else { Grade.style.color = "red"; }
            Grade.innerText = per + "%"

            return true;
        }
        function NextQuestion() {
            var tab = $find('<%=WebTab1.ClientID%>');
            var current = tab.get_selectedIndex();
            current = current + 1;
            if (current >= tab.get_tabs().length - 1) {
                var b = document.getElementById('bFoward');
                b.disabled = true;
            }
            tab.set_selectedIndex(current);
            var b = document.getElementById('bBack');
            b.disabled = false;
            var b = document.getElementById('lCounter');
            current = current + 1;
            b.innerText = 'Question ' + current + ' of ' + tab.get_tabs().length;
        }
        function PreviousQuestion() {
            var tab = $find('<%=WebTab1.ClientID%>');
            var current = tab.get_selectedIndex();
            current = current - 1;
            tab.set_selectedIndex(current);
            if (current == 0) {
                var b = document.getElementById('bBack');
                b.disabled = true;
            }
            var b = document.getElementById('bFoward');
            b.disabled = false;
            var b = document.getElementById('lCounter');
            current = current + 1;
            b.innerText = 'Question ' + current + ' of ' + tab.get_tabs().length;
        }
       
    </script>
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="Panel1" runat="server"  
            style="z-index: 1; width: 100%;height: 54px;" 
            BackColor="#99CCFF" >
            <asp:Label ID="lGrade" runat="server" Font-Bold="True" Font-Size="14pt" 
                ForeColor="Green"   
                style="position:absolute;margin-left:10px;margin-top:16px; width:100%; text-align:center"></asp:Label>
        <asp:Button ID="bCancel" runat="server" style="position:absolute; right:75px; margin-left:0px; margin-top: 8px; margin-bottom: 1px; top: 18px;"
            Text="Cancel" Width="60px" />
        <asp:Button ID="bGrade" runat="server" style="position:absolute; right:15px; margin-left:0px; margin-top: 8px; margin-bottom: 1px; top: 18px;"
            Text="Done" BackColor="Lime" Width="50px" />
            <asp:Label ID="lTitle" runat="server" Font-Bold="True" Font-Size="14pt" 
                ForeColor="Maroon" 
                style="position:absolute;margin-left:10px;margin-top:16px;" 
                Text="Label"></asp:Label>
        </asp:Panel>
        <input id="bBack" type="button" value="&lt;&lt;Back" 
           style="margin: 10px 20px;" onclick="return PreviousQuestion()" disabled="true" />
        <input id="bFoward" type="button" value="Next &gt;&gt;" 
            style="margin: 10px 20px 10px 0px" 
            onclick="return NextQuestion()" />
        <asp:Label ID="lCounter" runat="server"
            Text="Queston" Font-Size="12pt"></asp:Label>
            <ig:WebTab ID="WebTab1" runat="server" Height="459px" Width="100%"></ig:WebTab>
        <asp:HiddenField ID="hTestXML" runat="server" />
        <asp:HiddenField ID="hQuestionCount" runat="server" />
        <asp:HiddenField ID="hCorrectCount" runat="server" />
       <ig:WebScriptManager ID="WebScriptManager1" runat="server">
        </ig:WebScriptManager>
    </div>
    </form>
</body>
</html>
